package exerices;

import java.util.ArrayList;

import com.github.javafaker.Faker;

public class ex1 {

	/*
	Exercise 1 
		Write a bubble sort method that can sort an ArrayList<String> containing strings. 
		Test your method in a test class with a main method. 
	 */
	public static void main(String[] args) {

		Faker f = new Faker();
		ArrayList<String> dictionary = new ArrayList<>();
		for (int i = 0; i < 50; i++) {
			dictionary.add(f.lorem().word());
		}

		System.out.println("Unsorted: " + dictionary.toString());
		System.out.println("Sorted: " + sortListAlphabetic(dictionary).toString());

	}

	public static ArrayList<String> sortListAlphabetic(ArrayList<String> liste) {
		for (int i = liste.size() - 1; i >= 1; i--) {
			for (int j = 1; j <= i; j++) {
				if (liste.get(j - 1).compareTo(liste.get(j)) > 0) {
					String tmp = liste.get(j - 1);
					liste.set(j - 1, liste.get(j));
					liste.set(j, tmp);
				}
			}
		}
		return liste;
	}

}
