package ex3;

public class PersonApp {

	public static void main(String[] args) {
		/*
		 * d. Add a method public void printPerson() to the Person class. The method
		 * must print the persons name, address and monthly salary. Test that the method
		 * is working. e. Add a new method to the person class. The method must
		 * calculate the yearly salary of a person (12 times the monthly salary plus
		 * 2,5% in bonus). Come up with a suitable name
		 */

		Person person1 = new Person("Alexander", "Moon 1st", 9001);
		person1.printPerson();
		System.out.println("\n");
		person1.setName("Buzz Aldrin");
		person1.setAddress("Moon 2nd");
		person1.setMonthlySalary(32);
		person1.printPerson();
	}

}
