package model;

/**
 * This class models an employee.
 */
/*
 * e) Change the Employee class, so that an employee has both first firstName
 * and a last firstName. Change the constructor to allow a first firstName and a
 * last firstName. Add get and set methods that returns and set the new fields.
 * Test the methods.
 * 
 */
public class Employee {
	// The last name of the employee.
	private String firstName;
	// The last name of the employee.
	private String lastName;
	// Whether the employee is a trainee or not.
	private boolean trainee;
	// Employees age
	private int age;

	/**
	 * Creates an employee as a trainee with the given firstName.
	 */
	public Employee(String firstName, String lastName, Integer age) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.age = age;
		this.trainee = true;
	}

	/**
	 * Returns employee full name
	 */
	public String getName() {
		return firstName + " " + lastName;
	}

	/**
	 * Sets the firstName of the employee.
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * Returns the first name of the employee.
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * Sets the firstName of the employee.
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * Returns the first name of the employee.
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * Sets the age of the employee.
	 */
	public void setAge(Integer age) {
		this.age = age;
	}

	/**
	 * Returns the age of the employee.
	 */
	public Integer getAge() {
		return age;
	}

	/*
	 * Adds 1 to age if has birthday
	 */
	public void hasBirthday(boolean hasBirthday) {
		if (hasBirthday) {
			age += 1;
		}
	}

	/**
	 * Sets the trainee state of the employee.
	 */
	public void setTrainee(boolean trainee) {
		this.trainee = trainee;
	}

	/**
	 * Returns the trainee state of the employee.
	 */
	public boolean isTrainee() {
		return trainee;
	}

	/**
	 * Returns a description of the employee (for test purposes).
	 */
	@Override
	public String toString() {
		return firstName + " (trainee: " + trainee + ")";
	}

	/**
	 * Prints a description of the employee.
	 */
	public void printEmployee() {
		System.out.println("*******************");
		System.out.println("First name: " + firstName);
		System.out.println("Last name: " + lastName);
		System.out.println("Full name: " + this.getName());
		System.out.println("Trainee: " + trainee);
		System.out.println("Age: " + age);
		System.out.println("*******************");
		System.out.println();
	}
}
