package app.model;

import java.time.LocalTime;

public class Invalideplads extends Parkeringsplads {
	// FEJL: nedarver ikke statiske variable
	private final double PRICE_PER_10MIN = 0; // unused but clarifying that these places are free

	private double areal;

	public Invalideplads(int nummer, Parkeringshus parkeringsHus, double areal) {
		super(nummer, parkeringsHus);
		this.areal = areal;
	}

	public double getAreal() {
		return areal;
	}

	public void setAreal(double areal) {
		this.areal = areal;
	}

	@Override
	public double getPris(LocalTime afgang) {
		return 0.0;
	}

}
