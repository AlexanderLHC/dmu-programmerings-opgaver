package app.model;

import java.time.LocalTime;

import app.controller.Controller;

public class Tester {

	public static void main(String[] args) {
		// Kommentar: unødvendig fil men rar til eget brug
		Controller.createSomeObjects();
		Parkeringshus hus = Controller.getParkeringshuse().get(0);

		Controller.writeOptagnePladser(hus, "udskrift");

		// Henter bil efter 20 min
		System.out.println("Saldo før afhentning: " + hus.getSaldo());
		Parkeringsplads parkering = hus.getParkeringspladser().get(1);
		parkering.hentBil(LocalTime.now().plusMinutes(20));
		System.out.println("Saldo efter 20min afhentning: " + hus.getSaldo());

	}

}
