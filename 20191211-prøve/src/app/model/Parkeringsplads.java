package app.model;

import java.time.LocalTime;
import java.time.temporal.ChronoUnit;

public class Parkeringsplads {
	private int nummer;
	private LocalTime ankomst;
	private Parkeringshus parkeringsHus;
	private Bil bil;
	private final double PRICE_PER_10MIN = 6;

	public Parkeringsplads(int nummer, Parkeringshus parkeringsHus) {
		ankomst = null;
		this.nummer = nummer;
		this.parkeringsHus = parkeringsHus;
	}

	public int getNummer() {
		return nummer;
	}

	public void setNummer(int nummer) {
		this.nummer = nummer;
	}

	public LocalTime getAnkomst() {
		return ankomst;
	}

	public void setAnkomst(LocalTime ankomst) {
		this.ankomst = ankomst;
	}

	public Parkeringshus getParkeringsHus() {
		return parkeringsHus;
	}

	public void setParkeringsHus(Parkeringshus parkeringsHus) {
		if (this.parkeringsHus != parkeringsHus) {
			this.parkeringsHus = parkeringsHus;
		}
	}

	public Bil getBil() {
		return bil;
	}

	public void setBil(Bil bil) {
		if (this.bil != bil) {
			ankomst = LocalTime.now();
			this.bil = bil;
		}
	}

	// Kommentar:
	/* Nogle lagde den i Pakeringshus med Plads som param,
	 "Bør" høre til herinde.
	 Mange har:
	 - fået timer
	 - fået min
	*/
	public double getPris(LocalTime afgang) {
		long minutesBetween = ChronoUnit.MINUTES.between(ankomst, afgang);
		// Kommentar: hvis ikke Math.ceil husk da at lægge 1 til
		// for at få første time med
		return Math.ceil(minutesBetween / 10) * PRICE_PER_10MIN;
	}

	public void hentBil(LocalTime afgang) {
		parkeringsHus.insertSaldo(getPris(afgang));
		setBil(null);
	}
}
