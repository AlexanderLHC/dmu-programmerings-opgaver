package exercisesPersoner;

import java.time.LocalDate;
import java.util.Date;

public class Mekaniker extends Person{
	private LocalDate svendeprøveDate;
	private static Double timeløn = 172.0; // kr
	private static Double arbejdstimer = 37.0; 
	
	public Mekaniker(String navn, String adresse, LocalDate svendePrøveDate) {
		super(navn, adresse);
		this.svendeprøveDate = svendePrøveDate;
	}

	public Double getTimeløn() {
		return timeløn;
	}

	public void setTimeløn(Double timeløn) {
		this.timeløn = timeløn;
	}

	public LocalDate getSvendeprøveDate() {
		return svendeprøveDate;
	}	
	
	public Double ugeLøn() {
		return timeløn*arbejdstimer;
	}
	
	public static Double getArbejdstimer() {
		return arbejdstimer;
	}
	
	@Override
	public String toString() {
		return super.toString() + String.format(" %-10s | %-5skr | %-10skr |", svendeprøveDate, timeløn, ugeLøn());
	}
}