package minihandel;

public class PercentDiscount extends Discount {
	private int discountPercentage;

	public PercentDiscount(int discountPercentage) {
		this.discountPercentage = discountPercentage;
	}

	@Override
	public Double getDiscount(Double price) {
		return price * discountPercentage / 100;
	}
}
