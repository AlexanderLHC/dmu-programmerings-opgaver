package loops;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class Exercise1 extends Application {
	public static void main(String[] args) {
		Application.launch(args);
	}

	@Override
	public void start(Stage stage) {
		GridPane root = this.initContent();
		Scene scene = new Scene(root);

		stage.setTitle("Loops");
		stage.setScene(scene);
		stage.show();
	}

	private GridPane initContent() {
		GridPane pane = new GridPane();
		Canvas canvas = new Canvas(400, 400);
		pane.add(canvas, 0, 0);
		this.drawShapes(canvas.getGraphicsContext2D());
		return pane;
	}

	// ------------------------------------------------------------------------

	private void drawShapes(GraphicsContext gc) {
		// ======== Exercise 1 ======== //
		// a) Make an application that draws two arrowheads at (100,75) and (100,125).
		int x = 100;
		int y1 = 75;
		int y2 = 125;
		int arrowLength = 10 * 2;
		int arrowHeight = 4 * 2;
		// arrow 1
		gc.strokeLine(x, y1, x + arrowLength, y1 - arrowHeight);
		gc.strokeLine(x, y1, x + arrowLength, y1 + arrowHeight);
		// arrow 2
		gc.strokeLine(x, y2, x + arrowLength, y2 - arrowHeight);
		gc.strokeLine(x, y2, x + arrowLength, y2 + arrowHeight);

		// b) Add some code, so the application shows a third arrowhead at (20,50).
		// arrow 3
		int x3 = 20;
		int y3 = 50;
		gc.strokeLine(x3, y3, x3 + arrowLength, y3 - arrowHeight);
		gc.strokeLine(x3, y3, x3 + arrowLength, y3 + arrowHeight);

		// c) Change the code, so all the arrowheads have twice the length.
		// int arrowLength = 10*2;
		// int arrowHeight = 4 * 2;

		// d) How could you use variables to make it easy to change the size (both
		// horizontally and vertically) of all arrowheads?
		// (You can assume that all arrowheads have the same size.)
		// wuups.. solved in c)
	}

}
