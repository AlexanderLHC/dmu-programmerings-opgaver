package loops;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class Exercise3 extends Application {
	public static void main(String[] args) {
		Application.launch(args);
	}

	@Override
	public void start(Stage stage) {
		GridPane root = this.initContent();
		Scene scene = new Scene(root);

		stage.setTitle("Loops");
		stage.setScene(scene);
		stage.show();
	}

	private GridPane initContent() {
		GridPane pane = new GridPane();
		Canvas canvas = new Canvas(400, 400);
		pane.add(canvas, 0, 0);
		this.drawShapes(canvas.getGraphicsContext2D());
		return pane;
	}

	// ------------------------------------------------------------------------

	private void drawShapes(GraphicsContext gc) {
		// ======== Exercise 3 ======== //

		// Create 3 applications that draw the following figures.
		int f1x = 10;
		int f1y1 = 100;
		int f1y2 = 200;

		// figure 1
		while (f1x <= 60) {
			gc.strokeLine(f1x, f1y1, f1x, f1y2);
			f1x += 10;
		}
		// figure 2
		int f2x1 = 10;
		int f2x2 = 100;
		int f2y = 10;
		while (f2y <= 60) {
			gc.strokeLine(f2x1, f2y, f2x2, f2y);
			f2y += 10;
		}
		// figure 3
		int f3x1 = 200;
		int f3x2 = 220;
		int f3y = 10;
		while (f3y <= 60) {
			gc.strokeLine(f3x1, f3y, f3x2, f3y);
			f3x1 -= 10;
			f3x2 += 10;
			f3y += 10;
		}

	}

}
