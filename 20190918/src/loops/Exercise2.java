package loops;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class Exercise2 extends Application {
	public static void main(String[] args) {
		Application.launch(args);
	}

	@Override
	public void start(Stage stage) {
		GridPane root = this.initContent();
		Scene scene = new Scene(root);

		stage.setTitle("Loops");
		stage.setScene(scene);
		stage.show();
	}

	private GridPane initContent() {
		GridPane pane = new GridPane();
		Canvas canvas = new Canvas(400, 400);
		pane.add(canvas, 0, 0);
		this.drawShapes(canvas.getGraphicsContext2D());
		return pane;
	}

	// ------------------------------------------------------------------------

	private void drawShapes(GraphicsContext gc) {
		// ======== Exercise 2 ======== //
		// Create an application like the one above,
		// but this time with 9 lines and with a shared start point at (100,100)
		int x1 = 100;
		int x2 = 45;
		int y1 = 100;
		int y2 = 000;
		int amountLines = 9;
		int increaseGap = 10;
		while (x2 < 80 + amountLines * increaseGap) {
			gc.strokeLine(x1, y1, x2, y2);
			x2 += increaseGap;
		}

	}

}
