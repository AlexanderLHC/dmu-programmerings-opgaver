package loops;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class Exercise4 extends Application {
	public static void main(String[] args) {
		Application.launch(args);
	}

	@Override
	public void start(Stage stage) {
		GridPane root = this.initContent();
		Scene scene = new Scene(root);

		stage.setTitle("Loops");
		stage.setScene(scene);
		stage.show();
	}

	private GridPane initContent() {
		GridPane pane = new GridPane();
		Canvas canvas = new Canvas(1000, 1000);
		pane.add(canvas, 0, 0);
		this.drawShapes(canvas.getGraphicsContext2D());
		return pane;
	}

	// ------------------------------------------------------------------------

	private void drawShapes(GraphicsContext gc) {
		// ======== Exercise 4 ======== //
		// Create the figures shown below. Hints for the code can be found in example 6.

		// figure 1
		int f1x = 200;
		int f1y = 200;
		int r1 = 10;
		while (r1 <= 60) {
			gc.strokeOval(f1x - r1, f1y - r1, 2 * r1, 2 * r1);
			r1 += 10;
		}
		// figure 2
		int f2x = 200;
		int f2y = 400;
		int r2 = 10;
		while (r2 <= 60) {
			gc.strokeOval(f2x - r2, f2y - r2, 2 * r2, 2 * r2);
			f2x += 10;
			r2 += 10;
		}
		// figure 3
		int f3x = 200;
		int f3y = 500;
		int r3 = 50;
		int shifter = 0;

		while (shifter <= 300) {
			gc.strokeOval(f3x - shifter / 2, f3y, 2 * r3 + shifter, 2 * r3);
			shifter += 50;
		}
	}

}
