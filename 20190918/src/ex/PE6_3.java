/**
 * 
 */
package ex;

import java.util.Scanner;

/**
 * @author alexander
 *
 */
public class PE6_3 {

	/**
	 * @param args
	 */

	/*  E6.3 
	 	Write programs that read a line of input as a string and print
	
		a.Only the uppercase letters in the string.
		b.Every second letter of the string.
		c.The string, with all vowels replaced by an underscore.
		d.The number of vowels in the string.
		e.The positions of all vowels in the string. 
	
	*/
	public static void main(String[] args) {

//		exA();
//		exB();
//		exC();
//		exD();
		exE();
//
	}

	public static void exA() {
		// === a ==== //
//		a.Only the uppercase letters in the string.
		Scanner scan = new Scanner(System.in);
		System.out.println("a) alle store bogstaver i sætning. Skriv: ");
		String input = scan.nextLine();
		String capLetters = "";
		int i = 0;
		while (i < input.length()) {
			if ((char) input.charAt(i) >= 65 && (char) input.charAt(i) <= 90) {
				capLetters += input.charAt(i) + " ";
			}
			i++;
		}
		System.out.println("Store bogstaver: " + capLetters);
	}

	public static void exB() {
		// === b ==== //
//		b.Every second letter of the string.
		Scanner scan = new Scanner(System.in);
		System.out.println("b) hver andet bogstav. Skriv: ");
		String input = scan.nextLine();
		String lettersChosen = "";
		int i = 0;
		while (i < input.length()) {
			if (i % 2 != 0) {
				lettersChosen += input.charAt(i);
			}
			i++;
		}
		System.out.println("Færdig. Hver andet bogstav: " + lettersChosen);
	}

	public static void exC() {
		// === c ==== //
//		c.The string, with all vowels replaced by an underscore.
		Scanner scan = new Scanner(System.in);
		System.out.println("c) alle vokaler erstattet med understreg. Skriv: ");
		String input = scan.nextLine();
		String output = "";

		int i = 0;
		while (i < input.length()) {
			char curChar = input.charAt(i);
			// a, e, i, o, u, y, æ, ø, å
			if (curChar == 'a' || curChar == 'e' || curChar == 'i' || curChar == 'o' || curChar == 'u' || curChar == 'y'
					|| curChar == 'æ' || curChar == 'ø' || curChar == 'å') {
				output += "_";

			} else {
				output += curChar;
			}
			i++;
		}
		System.out.println("Forstår du?: " + output);
		System.out.println("Færdig.");
	}

	public static void exD() {
		// === d ==== //
//		d.The number of vowels in the string.
		Scanner scan = new Scanner(System.in);
		System.out.println("d) antallet af vokaler. Skriv: ");
		String input = scan.nextLine();
		int count = 0;
		int i = 0;

		while (i < input.length()) {
			char curChar = input.charAt(i);
			// a, e, i, o, u, y, æ, ø, å
			if (curChar == 'a' || curChar == 'e' || curChar == 'i' || curChar == 'o' || curChar == 'u' || curChar == 'y'
					|| curChar == 'æ' || curChar == 'ø' || curChar == 'å') {
				count++;
			}
			i++;
		}
		System.out.println("Antallet af vokaler: " + count);
		System.out.println("Færdig.");
	}

	public static void exE() {
		// === e ==== //
//		e.The positions of all vowels in the string. 
		Scanner scan = new Scanner(System.in);
		System.out.println("e) vokalers placering. Skriv: ");
		String input = scan.nextLine();
		String vowelPositions = "";
		int i = 0;

		while (i < input.length()) {
			char curChar = input.charAt(i);
			// a, e, i, o, u, y, æ, ø, å
			if (curChar == 'a' || curChar == 'e' || curChar == 'i' || curChar == 'o' || curChar == 'u' || curChar == 'y'
					|| curChar == 'æ' || curChar == 'ø' || curChar == 'å') {
				vowelPositions += i + 1 + " ";
			}
			i++;
		}
		System.out.println("Vokalernes placering: " + vowelPositions);
		System.out.println("Færdig.");
	}
}
