/**
 * 
 */
package ex;

import java.util.Scanner;

/**
 * @author alexander
 *
 */
public class PE6_1 {

	/**
	 * @param args
	 */

	/*  E6.1
	 * Write programs with loops that compute
		a.The sum of all even numbers between 2 and 100 (inclusive).
		b.The sum of all squares between 1 and 100 (inclusive).
		c.All powers of 2 from 20 up to 220.
		d.The sum of all odd numbers between a and b (inclusive), where a and b are inputs.
		e.The sum of all odd digits of an input. (For example, if the input is 32677, the sum would be 3 + 7 + 7 = 17.)
	*/
	public static void main(String[] args) {

//		exA();
//		exB();
//		exC();
//		exD();
//		exE();

	}

	public static void exA() {
//		// === a ==== //
		int i = 2;
		int sum = 0;
		while (i <= 100) {
			sum += i;
			i += 2;
		}
		System.out.println("a) Summen af alle lige tal fra 2-100 = " + sum);

	}

	public static void exB() {
		// // === b ==== //
		System.out.println("\n");
		int i = 0;
		int sum = 0;
		while (i <= 100) {
			sum += Math.pow(i, 2);
			i += 1;
		}
		System.out.println("b) summen af alle ^2 fra 1-100 = " + sum);
		i = 0;
		sum = 0;
		while (i <= 100) {
			if ((Math.log(i) / Math.log(2)) % 1 == 0) {
				sum += i;
			}
			i++;
		}
		System.out.println("b.2) summen af alle tal der kan beskrives ved ^2 fra 1-100 = " + sum);
	}

	public static void exC() {
		// // === c ==== //
//		c.All powers of 2 from 20 up to 220.
		System.out.println("\n");
		System.out.println("c) alle potenser på 2 fra ^0 til ^20 ");
		int exp = 0;
		while (exp <= 20) {
			System.out.println("2^ " + exp + ". " + Math.pow(2, exp));
			exp++;
		}

	}

	public static void exD() {
		// === d ==== //
		Scanner scan = new Scanner(System.in);
		System.out.println("\n");
//		// d.The sum of all odd numbers between a and b (inclusive), where a and b are
//		// inputs.
		System.out.println("d) Sum of all odd numbers between inputs");
		System.out.println("Vælg laveste tal i intervallet: ");
		int a = scan.nextInt();
		int aInput = a;
		System.out.println("Vælg højeste tal i intervallet: ");
		int b = scan.nextInt();
		int sum = 0;
		if (a < b) {
			while (a <= b) {
				if (a % 2 != 0) {
					sum += a;
				}
				a++;
			}
			System.out.println("d) summen af alle ulige tal fra " + aInput + " til " + b + " = " + sum);
		} else {
			System.out.println("FEJL: indtastede værdier var enten uegnede heltal eller forkert rækkefølge");
		}

	}

	public static void exE() {
		// === e ==== //
		// e.The sum of all odd digits of an input. (For example, if the input is 32677,
		// the sum would be 3 + 7 + 7 = 17.)
		Scanner scan = new Scanner(System.in);
		System.out.println("\n");
		System.out.println("e) Summen af alle ulige ciffre mellem");
		System.out.println("Vælg et tilfældigt tal: ");
		int randomNumber = scan.nextInt();
		System.out.println("\n");
		String oddNumbers = "";
		int sum = 0;
		while (randomNumber > 0) {
			if (randomNumber % 10 % 2 != 0) {
				oddNumbers += randomNumber % 10 + "+";
				sum += randomNumber % 10;
			}
			randomNumber = randomNumber / 10;
		}
		System.out
				.println("Summen af ulige ciffre (" + oddNumbers.substring(0, oddNumbers.length() - 1) + ") = " + sum);
	}
}
