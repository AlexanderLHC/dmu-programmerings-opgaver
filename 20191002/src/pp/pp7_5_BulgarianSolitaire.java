package pp;

import java.util.Random;

public class pp7_5_BulgarianSolitaire {

	/*
	 * P7.5
	  In this assignment, you will model the game of Bulgarian Solitaire.
	  The game starts with 45 cards.
	  (They need not be playing cards. Unmarked index cards work just as well.)
	  Randomly divide them into some number of piles of random size.
	  For example, you might start with piles of size 20, 5, 1, 9, and 10.
	  In each round, you take one card from each pile, forming a new pile with
	  these cards. 
	  For example, the sample starting configuration would be transformed into piles
	  of size 19, 4, 8, 9, and 5. 
	  The solitaire is over when the piles have size:
			   1, 2, 3, 4, 5, 6, 7, 8, and 9, in some order. 
	  (It can be shown that you always end up with such a configuration.)
	
	In your program, produce a random starting configuration and print it. Then keep applying the solitaire step and print the result. Stop when the solitaire final configuration is reached.
	
	
	 */
	public static void main(String[] args) {

		int[] endConfiguration = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		int pileSize = 45;
		int[] pile = new int[pileSize];
		int currentSize = 0;
		int steps = 0;

		//Form initial pile of random size >=1 and <=45
		for (int i = 0; pileSize > 0; i++) {
			Random random = new Random();
			int randomPileSize = random.nextInt(pileSize) + 1;
			pileSize -= randomPileSize;
			pile[i] = randomPileSize;
			currentSize++;
		}
		System.out.println("Amount of piles:" + currentSize);
		printProgress("First piles stack: ", pile, currentSize);

		while (true) {
			int pileNew = 0;
			for (int i = 0; i < currentSize; i++) {
				pile[i] = pile[i] - 1;
				pileNew++;
			}

			currentSize++;
			pile[currentSize - 1] = pileNew;

			currentSize = clearEmptyValues(pile, currentSize);
			sortBySize(pile, currentSize);
			printProgress("Pile: ", pile, currentSize);
			steps++;

			if (currentSize == 9) {
				sortBySize(pile, currentSize);
				boolean done = isItCorrect(pile, endConfiguration);

				if (done) {
					System.out.println("Pile have been ordered. Steps: " + steps);
					printProgress("Finally stack sorted: ", pile, currentSize);
					break;
				}

			}
		}
	}

	public static void printProgress(String text, int[] pile, int currentSize) {

		System.out.print(text);
		for (int i = 0; i < currentSize; i++) {
			System.out.printf("%d ", pile[i]);
		}
		System.out.println();
	}

	public static int clearEmptyValues(int[] pileSize, int currentSize) {
		for (int i = 0; i < currentSize; i++) {
			if (pileSize[i] == 0 && pileSize[currentSize - 1] != 0) {
				pileSize[i] = pileSize[currentSize - 1];
				currentSize--;
			} else if (pileSize[currentSize - 1] == 0) {
				currentSize--;
				i--;
			}
		}
		return currentSize;
	}

	public static void sortBySize(int[] pile, int currentSize) {
		for (int i = 0; i < currentSize; i++) {
			for (int j = i; j < currentSize; j++) {
				if (pile[j] < pile[i]) {
					int temp = pile[i];
					pile[i] = pile[j];
					pile[j] = temp;
				}
			}
		}
	}

	public static boolean isItCorrect(int[] pile, int[] goal) {

		for (int i = 0; i < goal.length; i++) {
			if (pile[i] != goal[i]) {
				return false;
			}
		}
		return true;
	}

}
