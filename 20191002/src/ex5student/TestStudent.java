package ex5student;

import java.util.Arrays;

public class TestStudent {
	/*
	 * Exercise 5
		a) Add a method public void addGrade(int grade) to the Student class. The method must add 
		the grade to the student (if the student's count of grades is less than the maximum count of 
		grades).
		b) Add a method public double gradeAverage() to the Student class. The method must returns 
		the grade average of the student.
		c) Add a method public int maxGrade() to the Student class. The method must return the 
		highest grade of the student. (Assume that gradeCount is >= 1.)
		d) Add a method public int minGrade() to the Student class. The method must return the 
		lowest grade of the student.
		e) Add a method public int[] getActualGrades() that returns an array containing exactly the 
		actual grades, the student has obtained.
		f) Test the methods in main() in TestStudent: Add some grades to the student(s) and call the 
		methods that return average, min and max. Also print out all the grades of the student(s).
	 */
	public static void main(String[] args) {
		Student s1 = new Student("Hanne");
		Student s2 = new Student("Margrethe");

		System.out.println(s1.getName());
		System.out.println(s2.getName());

		// a)
		System.out.println("a)");
		s1.addGrade(10);
		s1.addGrade(12);
		s1.addGrade(10);
		s1.addGrade(7);
		System.out.printf("Adding grade to %s. Grades array: %s%n", s1.getName(), Arrays.toString(s1.getGrades()));
		s2.addGrade(4);
		s2.addGrade(7);
		s2.addGrade(10);
		System.out.printf("Adding grade to %s. Grades array: %s%n", s2.getName(), Arrays.toString(s2.getGrades()));
		// b)
		System.out.println("b)");
		System.out.printf("%s average grade: %.2f%n", s1.getName(), s1.gradeAverage());
		System.out.printf("%s average grade: %.2f%n", s2.getName(), s2.gradeAverage());
		// c)
		System.out.println("c)");
		System.out.printf("%s highest grade: %d%n", s1.getName(), s1.maxGrade());
		System.out.printf("%s highest grade: %d%n", s2.getName(), s2.maxGrade());
		// d)
		System.out.println("d)");
		System.out.printf("%s lowest grade: %d%n", s1.getName(), s1.minGrade());
		System.out.printf("%s lowest grade: %d%n", s2.getName(), s2.minGrade());
		// e)
		System.out.println("e)");
		System.out.printf("%s. actual grades array: %s%n", s1.getName(), Arrays.toString(s1.getActualGrades()));
		System.out.printf("%s. actual grades array: %s%n", s2.getName(), Arrays.toString(s2.getActualGrades()));
	}
}
