package ex6student;

public class Child {
	private String name;
	private double[] weights;// can contain weights for the first 11 years
	private int weightsCount;

	public Child(String name) {
		this.name = name;
		this.weights = new double[11];
		this.weightsCount = 0;
	}

	public String getName() {
		return this.name;
	}

	public void addWeight(double weight) {
		weights[weightsCount] = weight;
		weightsCount++;
	}

	public double[] getWeights() {
		return this.weights;
	}

	public int getWeightsCount() {
		return this.weightsCount;
	}

	public double getWeightAtAge(int age) {
		return weights[age - 1]; // subtracting because array index starts 0
	}

	public String maxYearlyWeightGain() {
		double maxWeightGained = 0;
		String report = "";
		for (int i = 1; i < weightsCount; i++) {
			if (weights[i] - weights[i - 1] > maxWeightGained) {
				maxWeightGained = weights[i] - weights[i - 1];
				report = String.format("%d-%d. Increase %.2f", i - 1, i, maxWeightGained);
			}
		}
		return report;
	}
}
