package ex2Customers;

import java.util.Arrays;

public class CustomerTester {

	public static void main(String[] args) {
		Customer c1 = new Customer("Britt", "Bager", 43);
		Customer c2 = new Customer("Morten", "Østergaard", 43);
		Customer c3 = new Customer("Peter", "Østergaard", 43);
		Customer c4 = new Customer("Torsten", "Gejl", 55);

		Customer[] customers = {c1, c2, c3, c4};
		
		System.out.println("Customers: " + Arrays.toString(customers));
		System.out.println("Last customer (lastname > firstname > age): " + lastCustomer(customers));
		System.out.printf("Customers after %s: %s%n", c2, Arrays.toString(afterCustomer(customers, c2)));
		
	}

	public static Customer lastCustomer(Customer[] customers) {
		Customer lastCustomer = customers[0];
		
		for (Customer c : customers) {
			if (lastCustomer.compareTo(c) < 0) {
				lastCustomer = c;
			}
		}
		
		return lastCustomer;
	}
	
	public static Customer[] afterCustomer(Customer[] customers, Customer customer) {
//		Customer[] customersAfter = new Customer[customers.length];
//		int lastIndex = 0;
		boolean found = false;
		int i = 0;
		while (!found) {
			if (customers[i].equals(customer)) {
				found = true;
			} 
			i++;	

		}
			
//		while (i < customers.length) {
//			customersAfter[lastIndex] = customers[i];
//			lastIndex++;
//			i++;
//		}
		
		return Arrays.copyOfRange(customers, i, customers.length);
	}
}
