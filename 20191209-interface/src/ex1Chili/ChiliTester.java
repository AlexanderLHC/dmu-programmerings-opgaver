package ex1Chili;

import java.util.ArrayList;
import java.util.Arrays;

public class ChiliTester {

	public static void main(String[] args) {
		Chili c1 = new Chili();
		c1.setName("Red Savina Habanero");
		c1.setScoville(350000);
		
		Chili c2 = new Chili();
		c2.setName("Carolina Reaper");
		c2.setScoville(2200000);
		
		Chili c3 = new Chili();
		c3.setName("Naga Viper");
		c3.setScoville(1382118);
		
		Measurable[] chilis = { c1, c2, c3 };
		
		System.out.println("Chili list: " + Arrays.toString(chilis));
		System.out.println("Strongest chili = " + Data.max(chilis));
		System.out.println("Average scoville = " + Data.avg(chilis));
		System.out.println("Average scoville with filter = " + Data.avg(chilis));

		Filter chiliTester = new ChiliFilter(350001);
		Chili[] chilisList = {  c1, c2, c3 };
		System.out.println(Arrays.toString(getGreaterThan(chilisList, chiliTester)));
	}
	
	public static Chili[] getGreaterThan(Chili[] chilis, Filter filter) {
		Chili[] chilisFiltered = new Chili[chilis.length];
		int lastI = 0;
		for (Chili chili : chilis) {
			if (filter.accept(chili)) {
				chilisFiltered[lastI] = chili;
				lastI++;
			}
		}
		return Arrays.copyOf(chilisFiltered, lastI);
	}
}
