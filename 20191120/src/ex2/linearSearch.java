package ex2;

import java.util.Arrays;

import factory.Factory;

public class linearSearch {

	/*
	Write a linear search method that finds the first number belonging to
	 	the interval [10;15].
	The method must return the number found in the interval, 
	and take an array of integer numbers as parameter. If a number isn't found
	method must return -1;
	If the array is [7, 56, 34, 3, 7, 14, 13, 4], the method should return 14.
	 */
	public static void main(String[] args) {

		int[] list1 = Factory.getArray(10, 0, 100);
		int[] list2 = Factory.getArray(10, 0, 100);
		int[] list3 = Factory.getArray(10, 0, 100);
		int[] list4 = Factory.getArray(10, 0, 100);
		int[] list5 = { 7, 56, 34, 3, 7, 14, 13, 4 };

		int start = 10;
		int end = 15;
		System.out.println("First attempt: " + Arrays.toString(list1) + " = " +
				linearSearchListInInterval(list1, start, end));
		System.out.println("Second attempt: " + Arrays.toString(list2) + " = " +
				linearSearchListInInterval(list2, start, end));
		System.out.println("Third attempt: " + Arrays.toString(list3) + " = " +
				linearSearchListInInterval(list3, start, end));
		System.out.println("Fourth attempt: " + Arrays.toString(list4) + " = " +
				linearSearchListInInterval(list4, start, end));
		System.out.println("Fifth attempt: " + Arrays.toString(list5) + " = " +
				linearSearchListInInterval(list5, start, end));

	}

	private static int linearSearchListInInterval(int[] list, int start, int end) {
		int found = -1;
		int i = 0;
		while (i < list.length) {
			if (list[i] > start && list[i] < end) {
				found = list[i];
				break;
			}
			i++;
		}
		return found;
	}

}
