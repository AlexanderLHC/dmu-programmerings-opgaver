package ex3;

import java.util.Arrays;

import factory.Factory;

public class linearSearch {
	/*
	 Exercise 3
	Write a linear search method that returns true, if two adjacent numbers are the same. The 
	method must take an array of integer numbers as parameter. 
	Test the method.
	If the array is [7, 9, 13, 7, 9, 13],  the method must return false.
	If the array is [7, 9, 13, 13, 9, 7], the method must returns true.
	Write another method that returns true, if the same number exists in n adjacent places. The 
	method must take an array of integer numbers and the number n as parameters.
	Test the method.
	*/

	public static void main(String[] args) {
		int[] list1 = Factory.getArray(6, 0, 20);
		int[] list2 = Factory.getArray(6, 0, 20);
		int[] list3 = Factory.getArray(6, 0, 10);
		int[] list4 = { 7, 9, 13, 7, 9, 13 };
		int[] list5 = { 7, 9, 13, 13, 9, 7 };
		int[] list6 = { 7, 7, 0, 13, 9, 7 };
		int[] list7 = { 7, 7, 7, 13, 9, 7 };

		//		System.out.println("First attempt 2 adjacent: " + Arrays.toString(list1) + " = " +
		//				linearSearchListHasAdjacentInt(list1, 2));
		//		System.out.println("Second attempt 3 adjacent: " + Arrays.toString(list2) + " = " +
		//				linearSearchListHasAdjacentInt(list2, 3));
		//		System.out.println("Third attempt 3 adjacent: " + Arrays.toString(list3) + " = " +
		//				linearSearchListHasAdjacentInt(list3, 3));
		System.out.println("Fourth attempt 2 adjacent: " + Arrays.toString(list4) + " = " +
				linearSearchListHasAdjacentInt(list4, 2));
		System.out.println("Fifth attempt 2 adjacent: " + Arrays.toString(list5) + " = " +
				linearSearchListHasAdjacentInt(list5, 2));
		System.out.println("Sixth attempt 3 adjacent: " + Arrays.toString(list6) + " = " +
				linearSearchListHasAdjacentInt(list6, 3));
		System.out.println("Seventh attempt 3 adjacent: " + Arrays.toString(list7) + " = " +
				linearSearchListHasAdjacentInt(list7, 3));

	}

	private static boolean linearSearchListHasAdjacentInt(int[] list, int nAdjacent) {
		boolean found = false;
		int i = 0;
		while (i < list.length - nAdjacent + 1 && !found) {
			if (nAdjacentFound(list, i, nAdjacent)) {
				found = true;
			}
			i++;
		}
		return found;
	}

	private static boolean nAdjacentFound(int[] list, int index, int n) {
		int counter = 0;
		boolean found = false;

		// TODO: doesn't break
		while (counter < n && !found) {
			if (list[index + counter] == list[index + counter + 1]) {
				found = true;
			}
			counter++;
		}
		return found;
	}
}
