package ex1;

import java.util.Arrays;

import factory.Factory;

public class linearSearch {

	/*
	  	Write a linear search method that returns, whether an uneven number exists in an array. The 
		method must return true or false, and take an array of integer numbers as parameter.
		Test the method.
	 */
	public static void main(String[] args) {

		int[] list1 = getArray(10, 25);
		int[] list2 = getArray(5, 75);
		int[] list3 = getArray(5, 0);
		int[] list4 = getArray(5, 100);

		System.out.println("First attempt: " + Arrays.toString(list1) + " = " +
				linearSearchEvenessList(list1));

		System.out.println("Second attempt: " + Arrays.toString(list1) + " = " +
				linearSearchEvenessList(list2));

		System.out.println("Third attempt: " + Arrays.toString(list2) + " = " +
				linearSearchEvenessList(list3));

		System.out.println("Fourth attempt: " + Arrays.toString(list2) + " = " +
				linearSearchEvenessList(list4));
	}

	private static int[] getArray(int length, int chanceOdd) {
		int[] list = new int[length];
		for (int i = 0; i < length; i++) {
			list[i] = Factory.evenOrOdd(chanceOdd);
		}
		return list;
	}

	private static boolean linearSearchEvenessList(int[] list) {
		int i = 0;
		boolean evenFound = false;

		while (i < list.length) {
			if (list[i] % 2 == 0) {
				evenFound = true;
				break;
			}
			i++;
		}

		return evenFound;
	}

}