package ex4;

import java.util.Comparator;

public class Player {
	private String name;
	private int height;
	private double weight;
	private int scoredGoals;

	public Player(String name, int height, double weight, int scoredGoals) {
		this.name = name;
		this.height = height;
		this.weight = weight;
		this.scoredGoals = scoredGoals;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public int getScoredGoals() {
		return scoredGoals;
	}

	public void setScoredGoals(int scoredGoals) {
		this.scoredGoals = scoredGoals;
	}

	public static Comparator<Player> sortDescByScoredGoal = new Comparator<Player>() {

		@Override
		public int compare(Player p1, Player p2) {

			//sort in ascending order
			return p2.getScoredGoals() - p1.getScoredGoals();
			//			return obj1.name.compareTo(obj2.name);

			//sort in descending order
			//return obj2.name.compareTo(obj1.name);
		}
	};

	@Override
	public String toString() {
		return "[" + name + ". G: " + scoredGoals + ". H: " + height + "]";
	}

}
