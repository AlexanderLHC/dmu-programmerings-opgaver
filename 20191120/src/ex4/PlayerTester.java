package ex4;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

import com.github.javafaker.Faker;

public class PlayerTester {

	/*
	  
	Exercise 4
	In this exercise you must use a Player class. Make a player class with the following fields: 
	name, height, weight, scoredGoals. The class must have a constructor that initializes all the 
	fields, and get methods for all four fields (and a toString() method).
	In TestSearching make an ArrayList of Player with about 6-8 Players.
	a) Write a linear search method that returns a player with a given number of scored goals 
	found in an ArrayList of players given as parameter. Return null, if no such player is found.
	 
	The header of the method:
	public Player findPlayerLinear(ArrayList<Player> players, int score)
	 
	b) Write a binary search method that returns a player with a given number of scored goals 
	found in a sorted ArrayList of players (sorted in descending order according to scored goals) 
	given as parameter. Return null, if no such player is found.
	
	The header of the method: public Player findPlayerBinary(ArrayList<Player> players, int score)
	
	c) Write a linear search method that returns the name of  a player with a height less than 170 
	cm and with more than 50 scored goals in a given ArrayList of players. Return the empty 
	string, if no player is found.
	The header of the method:
	public String findPlayerName(ArrayList<Player> players)
	 */
	public static void main(String[] args) {
		Player[] players = new Player[50];
		//		Faker f = new Faker();
		Faker faker = new Faker();
		Random r = new Random();

		for (int i = 0; i < 50; i++) {
			players[i] = new Player(faker.funnyName().name(), r.nextInt(110) + 90, r.nextInt(90) * 1.0, r.nextInt(100));

		}
		ArrayList<Player> playersAList = new ArrayList<>();
		for (Player p : players) {
			playersAList.add(p);
		}

		// Makes sure player with 8 goals exist
		players[4].setScoredGoals(8);
		// Sorts descending by scored goals
		Collections.sort(playersAList, Player.sortDescByScoredGoal);

		System.out.printf("The array of players %s%n", playersAList.toString());
		System.out.println(
				"a) Looking for player who score " + 8 + " goals: " + linearSearchPlayerScoredGoal(players, 8));
		System.out.println("a) Looking for player who score " + 11 + " goals (expect fail): "
				+ linearSearchPlayerScoredGoal(players, 11));
		System.out.printf("b) Binary lookup in sorted array (8 goals). Result = %s%n",
				findPlayerBinary(playersAList, 8));
		System.out.printf("b) Binary lookup in sorted array (11 goals). Result = %s%n",
				findPlayerBinary(playersAList, 11));
		System.out.printf("c) Linear lookup (smaller than 170cm and greater than 50 goals). Result = %s%n",
				findPlayerName(playersAList, 170, 50));
		System.out.printf("c) Linear lookup (smaller than 10cm and greater than 50 goals). Result = %s%n",
				findPlayerName(playersAList, 10, 50));

	}

	private static boolean linearSearchPlayerScoredGoal(Player[] players, int goals) {
		boolean found = false;
		int i = 0;
		while (i < players.length) {
			if (players[i].getScoredGoals() == goals) {
				found = true;
			}
			i++;
		}
		return found;
	}

	private static Player findPlayerBinary(ArrayList<Player> players, int score) {
		Player isFound = null;
		int left = 0;
		int right = players.size() - 1;
		while (left <= right && isFound == null) {
			int middle = (left + right) / 2;
			Player candidate = players.get(middle);

			if (candidate.getScoredGoals() == score) {
				isFound = candidate;
			}
			if (candidate.getScoredGoals() - score > 0) {
				left = middle + 1;
			} else {
				right = middle - 1;
			}
		}
		return isFound;
	}

	public static String findPlayerName(ArrayList<Player> players, int height, int goals) {
		String name = "";

		int i = 0;
		while (i < players.size() && name.compareTo("") == 0) {
			if (players.get(i).getHeight() < height && players.get(i).getScoredGoals() > goals) {
				name = players.get(i).getName();
			}
			i++;
		}

		return name;
	}
}
