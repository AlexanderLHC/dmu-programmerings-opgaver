/**
 * 
 */
package ex;

/**
 * @author alexander
 *
 */
public class RE6_21 {

	/**
	 * @param args
	 */
	/*
	 * R6.21
	 * What do the following program segments print? 
	 * Find the answers by tracing the code, not by using the computer.
	 */
	public static void main(String[] args) {

		// a) = 4
		int n = 1;
		for (int i = 2; i < 5; i++) {
			n = n + i;
		}
		System.out.println("a) " + n);
		// b) = 6 (i doesn't change and skips reaching 6)
		int i;
		double p = 1 / 2;
		for (i = 2; i <= 5; i++) {
			p = p + 1.0 / i;
		}
		System.out.println("b) " + i);
		// c) = 3
		double x = 1;
		double y = 1;
		i = 0;
		do {
			y = y / 2;
			x = x + y;
			i++;
		} while (x < 1.8);
		System.out.println("c) " + i);
		// d) = 0
		x = 1;
		y = 1;
		i = 0;
		while (y >= 1.5) {
			x = x / 2;
			y = x + y;
			i++;
		}
		System.out.print("d) " + i);
	}

}
