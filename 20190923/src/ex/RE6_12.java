/**
 * 
 */
package ex;

/**
 * @author alexander
 *
 */
public class RE6_12 {

	/**
	 * @param args
	 */
	/*
	 * R6.12
	 * How many iterations do the following loops carry out?
	 * Assume that i is not changed in the loop body.
	 */
	public static void main(String[] args) {

		// a.for (int i = 1; i <= 10; i++) = 10 times
//		for (int i = 1; i <= 10; i++) {
//			System.out.println(i);
//		}

		// b.for (int i = 0; i < 10; i++) = 9
//		for (int i = 0; i < 10; i++) {
//			System.out.println(i);
//		}

//		c.for (int i = 10; i > 0; i--) = 10
//		for (int i = 10; i > 0; i--) {
//			System.out.println(i);
//		}
//		d.for (int i = -10; i <= 10; i++) = 20
//		for (int i = -10; i <= 10; i++) {
//			System.out.println(i);
//		}
//		e.for (int i = 10; i >= 0; i++) = forever
//		for (int i = 10; i >= 0; i++) {
//			System.out.println(i);
//		}
//		f.for (int i = -10; i <= 10; i = i + 2) = 10
//		for (int i = -10; i <= 10; i = i + 2) {
//			System.out.println(i);
//		}
//		g.for (int i = -10; i <= 10; i = i + 3) = 6
//		for (int i = -10; i <= 10; i = i + 3) {
//			System.out.println(i);
//		}
	}

}
