package loops;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class Exercise12 extends Application {
	public static void main(String[] args) {
		Application.launch(args);
	}

	@Override
	public void start(Stage stage) {
		GridPane root = this.initContent();
		Scene scene = new Scene(root);

		stage.setTitle("Loops");
		stage.setScene(scene);
		stage.show();
	}

	private GridPane initContent() {
		GridPane pane = new GridPane();
		Canvas canvas = new Canvas(500, 500);
		pane.add(canvas, 0, 0);
		this.drawShapes(canvas.getGraphicsContext2D());
		return pane;
	}

	// ------------------------------------------------------------------------

	private void drawShapes(GraphicsContext gc) {
		// ======== Exercise 12 ======== //
		/* 
			Create an application that draws a number of triangles. 
			You must code a method that draws a triangle.
			The triangle must have (x,y) and h as parameters (look at the figure). 
			
			Use your triangle-method to draw the following figures. 
			The size of the small triangles is 1/3 of the 
			big triangle. (h = 81 is used in the figure).  
			Hint: Make a method drawInnerTriangles() 
			that draws 3 triangles inside a triangle given by (x,y) 	
		*/
		double x = 150;
		double y = 250;
		double h = 81 * 2;
		boolean doubleInner = true;
		drawTriangle(gc, x, y, h);
		drawInnerTriangle(gc, x, y, h, 3, doubleInner);
	}

	/**
	 * 
	 * @param gc
	 * @param x: start coordinate
	 * @param y: start coordinate
	 * @param h: height
	 */
	private void drawTriangle(GraphicsContext gc, double x, double y, double h) {
		// Hypotenuse
		gc.strokeLine(x, y, x + h * 2, y);
		// Cathetus 1
		gc.strokeLine(x, y, x + h, y - h);
		// Cathetus 2
		gc.strokeLine(x + h, y - h, x + h * 2, y);
	}

	private void drawInnerTriangle(GraphicsContext gc, double x, double y, double h, double ratio,
			boolean doubleInner) {
		double heightSmallTri = h / ratio;
		// most left triangle
		drawTriangle(gc, x, y, heightSmallTri);
		// most right
		double triRightX = x + h * 2 - heightSmallTri * 2;
		drawTriangle(gc, triRightX, y, heightSmallTri);
		// top triangle
		double triTopX = x + h - heightSmallTri;
		double triTopY = y - h + heightSmallTri;
		drawTriangle(gc, triTopX, triTopY, heightSmallTri);

		if (doubleInner) {
			drawInnerTriangle(gc, x, y, heightSmallTri, 3, false);
			drawInnerTriangle(gc, triRightX, y, heightSmallTri, 3, false);
			drawInnerTriangle(gc, triTopX, triTopY, heightSmallTri, 3, false);
		}
	}

}
