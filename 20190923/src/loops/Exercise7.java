package loops;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class Exercise7 extends Application {
	public static void main(String[] args) {
		Application.launch(args);
	}

	@Override
	public void start(Stage stage) {
		GridPane root = this.initContent();
		Scene scene = new Scene(root);

		stage.setTitle("Loops");
		stage.setScene(scene);
		stage.show();
	}

	private GridPane initContent() {
		GridPane pane = new GridPane();
		Canvas canvas = new Canvas(1000, 1000);
		pane.add(canvas, 0, 0);
		this.drawShapes(canvas.getGraphicsContext2D());
		return pane;
	}

	// ------------------------------------------------------------------------

	private void drawShapes(GraphicsContext gc) {
		// ======== Exercise 7 ======== //
		// Text can be drawn with the method fillText().
		// As an example the code below will write the text ”Datamatiker” placed with
		// lower,
		// left corner at (20,50)
		// USE FOR LOOP
		String s = "Datamatiker";

		for (int i = 0; i <= s.length(); i++) {
			gc.fillText(s.substring(0, i), 20, 50 + 10 * i);
		}

	}

}
