package loops;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class Exercise11 extends Application {
	public static void main(String[] args) {
		Application.launch(args);
	}

	@Override
	public void start(Stage stage) {
		GridPane root = this.initContent();
		Scene scene = new Scene(root);

		stage.setTitle("Loops");
		stage.setScene(scene);
		stage.show();
	}

	private GridPane initContent() {
		GridPane pane = new GridPane();
		Canvas canvas = new Canvas(1000, 1000);
		pane.add(canvas, 0, 0);
		this.drawShapes(canvas.getGraphicsContext2D());
		return pane;
	}

	// ------------------------------------------------------------------------

	private void drawShapes(GraphicsContext gc) {
		// ======== Exercise 11 ======== //
		/* 
		 * Extend the application from exercise 10 with a method that draws a cross at (x,y) 
		 * with size w (size is the length of the lines in the cross).  
				+ 
		 * Use the method to draw crosses at the center of the circles. 
		*/
		int x = 50;
		int y = 100;
		int r = 40;
		while (x <= 150) {
			drawCircle(gc, x, y, r);
			drawCross(gc, x, y, r); // r represents length
			x += 25;
		}
	}

	/**
	 * 
	 * @param gc
	 * @param x: center coordinate
	 * @param y: center coordinate
	 * @param r: radius
	 */
	public void drawCircle(GraphicsContext gc, int x, int y, int r) {
		gc.strokeOval(x - r, y - r, 2 * r, 2 * r);
	}

	/**
	 * 
	 * @param gc
	 * @param x:      start coordinate
	 * @param y:      start coordinate
	 * @param length: length of line (from starting point)
	 */
	public void drawCross(GraphicsContext gc, int x, int y, int length) {
		gc.strokeLine(x, y - length, x, y + length);
		gc.strokeLine(x - length, y, x + length, y);
	}

}
