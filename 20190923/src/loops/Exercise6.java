package loops;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class Exercise6 extends Application {
	public static void main(String[] args) {
		Application.launch(args);
	}

	@Override
	public void start(Stage stage) {
		GridPane root = this.initContent();
		Scene scene = new Scene(root);

		stage.setTitle("Loops");
		stage.setScene(scene);
		stage.show();
	}

	private GridPane initContent() {
		GridPane pane = new GridPane();
		Canvas canvas = new Canvas(500, 500);
		pane.add(canvas, 0, 0);
		this.drawShapes(canvas.getGraphicsContext2D());
		return pane;
	}

	// ------------------------------------------------------------------------

	private void drawShapes(GraphicsContext gc) {
		// ======== Exercise 6 ======== //
		// Streg
		int x = 10;
		int y = 250;
		int lineLength = 450;
		gc.strokeLine(x, y, lineLength, y);
		// Pil
		arrow(gc, lineLength, y);
		// Koordinat indeling
		int splitsAmount = 11;
		int splitsGapSize = lineLength / splitsAmount;
		for (int i = 0; i < splitsAmount; i++) {
			if (i % 5 == 0) {
				gc.strokeLine(x + splitsGapSize * i, y + 10, x + splitsGapSize * i, y - 10);
				gc.fillText("" + i, x - 5 + splitsGapSize * i, y + 19);
			} else {
				gc.strokeLine(x + splitsGapSize * i, y + 5, x + splitsGapSize * i, y - 5);
			}
		}
	}

	private void arrow(GraphicsContext gc, int x, int y) {
		gc.strokeLine(x, y, x - 7, y + 4);
		gc.strokeLine(x, y, x - 7, y - 4);
	}

}
