package opg4;

import java.util.ArrayList;

/*
 * Opgave 2 
	her går ud på at vende navigeringen i opgave 1 om, så det nu er træningsplanen der kender 

	a. Implementer associeringen, givet at der kun er brug for at finde de tilknyttede svømmere ud 
		fra træningsplanen – ikke den anden vej! Hent inspiration i noten afsnit 2. 
	b. Lav en ny SwimmerApp klasse, som opretter to svømmere og en træningsplan og sætter 
		svømmerne som svømmere på planen. 
	c. Udvid SwimmingApp-klassen, så du ud fra træningsplanen finder de svømmere, som er 
		tilknyttet planen, og udskriver informationer om hver svømmer på skærmen.
	
 */

public class SwimmerAppOpg4 {

	public static void main(String[] args) {
		TrainingPlanOpg4 planA = new TrainingPlanOpg4('A', 16, 10);
		TrainingPlanOpg4 planB = new TrainingPlanOpg4('B', 10, 6);
		TrainingPlanOpg4 planC = new TrainingPlanOpg4('C', 8, 2);

		ArrayList<Double> lapTimes = new ArrayList<>();
		lapTimes.add(1.02);
		lapTimes.add(1.01);
		lapTimes.add(0.99);
		lapTimes.add(0.98);
		lapTimes.add(1.02);
		lapTimes.add(1.04);
		lapTimes.add(0.99);
		SwimmerOpg4 s1 = planA.createSwimmer("Jan", 1994, lapTimes, "AGF");

		lapTimes = new ArrayList<>();
		lapTimes.add(1.05);
		lapTimes.add(1.01);
		lapTimes.add(1.04);
		lapTimes.add(1.06);
		lapTimes.add(1.08);
		lapTimes.add(1.04);
		lapTimes.add(1.02);
		SwimmerOpg4 s2 = planA.createSwimmer("Bo", 1995, lapTimes, "Lyseng");

		lapTimes = new ArrayList<>();
		lapTimes.add(1.03);
		lapTimes.add(1.01);
		SwimmerOpg4 s3 = planB.createSwimmer("Mikkel", 1993, lapTimes, "AITA-Tranbjerg");

		System.out.println(planA);
		System.out.println(planB);
		System.out.println(planC);

		System.out.println("Removing swimmer: " + s1.getName() + " from " + planA.getLevel());
		planA.removeSwimmer(s1);
		System.out.println(planA);
	}

}
