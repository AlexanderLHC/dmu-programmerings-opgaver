package ex3;

public class Exercise3 {

	/*
	 * Exercise 3
		The UML diagram here shows two classes: 
		Team and Player (multipliciteter: 0..1 → 0..*).
	
		a) Add the Team and Player classes to a project in Eclipse. 
			Add the fields shown in the diagram to the classes. 
		b) Add to the Player class:
		– a constructor that initializes name and age from parameters and sets 
			score to 0,
		– get methods for name and age,
		– get and set methods for score,
		– a  void addScore(int score) method that adds the value in the parameter 
			to the players score.
		c) Add to the Team class:
		– a constructor that initializes name from a parameter and initializes 
			players to an empty ArrayList<Player> object,
		– get methods for name and players,
		– a method void addPlayer(Player p) that adds a player to the ArrayList of players,
		– a method void printPlayers() that prints the name, the age and the score for 
			all players with one player on each line (use a for-each loop),
		– a method double calcAverageAge() that returns the average age all the 
			players (use a for-each loop),
		– a method  int calcTotalScore()  that returns the total score of all players (use 
			a for-each loop),
		– a method  int calcOldPlayersScore(int ageLimit)  that returns the 
			total score of players older than ageLimit (use a for-each loop),
		– a method  int maxScore() that returns the highest score obtained by any player 
			(use a for-each loop),
		– a method  ArrayList<String> bestPlayerNames()  that returns an 
			ArrayList with the names of the players with the highest score (use a for-each loop).
		d) Add a class with a main() method to the project. Add code in the main() method to test all 
			the methods above (with a minimum of 4 players)
		 */
	public static void main(String[] args) {
		Player warhol = new Player("Andy Warhol", 32);
		Player picasso = new Player("Pablo Picasso", 33);
		Player gogh = new Player("Vincent van Gogh", 23);
		Player munch = new Player("Edvard Munch", 23);
		System.out.printf("Name %s is %d and has scored %d.%n", warhol.getName(), warhol.getAge(), warhol.getScore());
		warhol.addScore(200);
		picasso.addScore(9000);
		gogh.addScore(10);
		munch.addScore(800);

		Team creativeMinds = new Team("Creative Minds");

		creativeMinds.addPlayer(warhol);
		creativeMinds.addPlayer(picasso);
		creativeMinds.addPlayer(gogh);
		creativeMinds.addPlayer(munch);
		System.out.printf("Team: %s. Consist of: %n", creativeMinds.getName());
		creativeMinds.printPlayers();

		System.out.printf("Teams average age: %f.%n", creativeMinds.calcAverAge());

		System.out.printf("Teams total score: %d.%n", creativeMinds.calcTotalScore());

		System.out.printf("Sum of players score who is older than 30: %d.%n", creativeMinds.calcOldPlayersScore(30));

		System.out.printf("Teams highest score: %d.%n", creativeMinds.maxScore());

		System.out.printf("Teams highscore: %s.%n", creativeMinds.bestPlayerNames());

	}

}
