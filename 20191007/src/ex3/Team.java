package ex3;

import java.util.ArrayList;

public class Team {
	private String name;
	private ArrayList<Player> players;

	public Team(String name) {
		this.name = name;
		players = new ArrayList<>();
	}

	public String getName() {
		return name;
	}

	public ArrayList<Player> getPlayers() {
		//		Wups
		//		ArrayList<String> names = new ArrayList<>();
		//		for (Player player : players) {
		//			names.add(player.getName());
		//		}
		//		return names;
		return players;
	}

	public void addPlayer(Player p) {
		players.add(p);
	}

	public void printPlayers() {
		System.out.println("Players on the team: " + getName());
		for (Player p : players) {
			System.out.printf("Player %s (%d years) has scored %d%n", p.getName(), p.getAge(), p.getScore());
		}
	}

	public double calcAverAge() {
		int ageSum = 0;
		for (Player p : players) {
			ageSum += p.getAge();
		}
		return ageSum * 1.0 / players.size();
	}

	public int calcTotalScore() {
		int sum = 0;
		for (Player p : players) {
			sum += p.getScore();
		}
		return sum;
	}

	public int calcOldPlayersScore(int ageLimit) {
		int sum = 0;
		for (Player p : players) {
			if (p.getAge() > ageLimit) {
				sum += p.getAge();
			}
		}
		return sum;
	}

	public int maxScore() {
		int maxScore = 0;
		for (Player p : players) {
			if (maxScore < p.getScore()) {
				maxScore = p.getScore();
			}
		}
		return maxScore;
	}

	public ArrayList<String> bestPlayerNames() {
		ArrayList<String> bestPlayers = new ArrayList<>();
		for (int i = 0; i < players.size() - 1; i++) {
			for (int j = 0; j < players.size() - i - 1; j++) {
				if (players.get(j).getScore() > players.get(j + 1).getScore()) {
					Player tmp = players.get(j);
					players.remove(j);
					players.add(j + 1, tmp);
				}
			}
		}
		for (int i = 0; i < players.size(); i++) {
			bestPlayers.add("" + players.get(i).getName() + ":" + players.get(i).getScore() + ". ");

		}
		return bestPlayers;
	}

}
