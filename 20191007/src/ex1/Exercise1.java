package ex1;

import java.util.ArrayList;

public class Exercise1 {

	/*
	 * Exercise 1
		Create a class with a main() method. 
		Add code in the main() method to do the following: 
		1. Create an ArrayList that can contains objects of type String.
		2. Add strings to the list (in the given order): 
			* ”Hans”
			* ”Viggo”
			* ”Jens”
			* ”Bente”
			* ”Bent”
		3. Print the size of the list.
		4. Add ”Jane” at index 2 in the list.
		5. Print the elements in the list.
		6. Remove the element at index 1.
		7. Add ”Pia” at the front of the list. 
		8. Add ”Ib” at the rear of the list. 
		9. Print the size of the list.
		10. Replace the element at index 2 with ”Hansi”. 
		11. Print the size of the list.
		12. Print the elements in the list. 
		13. Traverse the list with a for statement and print the length of each element in the list.
		14. Traverse the list with a for-each statement and print the length of each element in the 
	 */
	public static void main(String[] args) {
		//		1. Create an ArrayList that can contains objects of type String.
		ArrayList<String> names = new ArrayList<>();
		//		2. Add strings to the list (in the given order): 
		names.add("Hans");
		names.add("Viggo");
		names.add("Jens");
		names.add("Bente");
		names.add("Bent");
		//		3. Print the size of the list.
		System.out.println("Size of names ArrayList: " + names.size());
		//		4. Add ”Jane” at index 2 in the list.
		names.add(2, "Jane");
		//		5. Print the elements in the list.
		System.out.println("Names ArrayList contains: " + names.toString());
		//		6. Remove the element at index 1.
		names.remove(1);
		//		7. Add ”Pia” at the front of the list. 
		names.add(0, "Pia");
		//		8. Add ”Ib” at the rear of the list. 
		names.add("Ib");
		//		9. Print the size of the list.
		System.out.println("Size of names ArrayList: " + names.size());
		//		10. Replace the element at index 2 with ”Hansi”. 
		names.set(2, "Hansi");
		//		11. Print the size of the list.
		System.out.println("Size of names ArrayList: " + names.size());
		//		12. Print the elements in the list. 
		System.out.println("Names ArrayList contains: " + names.toString());
		//		13. Traverse the list with a for statement and print the length of each element in the list.
		for (int i = 0; i < names.size(); i++) {
			System.out.println("Name: " + names.get(i) + " is " + names.get(i).length() + " characters long.");
		}
		//		14. Traverse the list with a for-each statement and print the length of each element in the 
		for (String name : names) {
			System.out.println("Name: " + name + " is " + name.length() + " characters long.");
		}

	}

}
