/**
 * 
 */
package ex;

/**
 * @author alexander
 *
 */
public class Decryptor {
	private static int ALPHABET_END = 90;
	private static int ALPHABET_START = 65;
	private String messageEncrypted;
	private String message;
	int distort;

	public Decryptor(String messageEncrypted, int distort) {
		this.messageEncrypted = messageEncrypted;
		this.distort = distort;
		decryptMessage();
	}

	public String getMessage() {
		return message;
	}

	public void setMessageEncrypted(String messageEncrypted) {
		this.messageEncrypted = messageEncrypted;
		decryptMessage();
	}

	public void decryptMessage() {
		String messageDuringDecryption = "";
		int distortCount = 0;
		for (char ch : messageEncrypted.toCharArray()) {
			messageDuringDecryption += String.valueOf(getUnscrambledChar(ch, distortCount));
			distortCount++;
		}
		message = messageDuringDecryption;
	}

	private char getUnscrambledChar(char ch, int distortCount) {
		int distortSum = distort + distortCount;
		char scrambledChar = (char) (((int) ch - distortSum - ALPHABET_END) % ALPHABET_START + ALPHABET_END);
//		System.out.println(ch + " " + (int) ch + " " + scrambledChar);
		return scrambledChar;
	}

}
