package ex1;

public class Dog {
	private String name;
	private boolean hasPedigree;
	private int price;
	private Race race;

	public Dog(String name, boolean hasPedigree, int price, Race race) {
		this.name = name;
		this.hasPedigree = hasPedigree;
		this.price = price;
		this.race = race;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isHasPedigree() {
		return hasPedigree;
	}

	public void setHasPedigree(boolean hasPedigree) {
		this.hasPedigree = hasPedigree;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public Race getRace() {
		return race;
	}

	public void setRace(Race race) {
		this.race = race;
	};

}
