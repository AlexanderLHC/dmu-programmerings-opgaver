package semesterprøve2017;

import java.util.ArrayList;

public class Plads {
	private int nr;
	private Område område;
	private ArrayList<Reservation> reservationer;
	private static double standardTimePris = 50; // kr

	public Plads(int nr, Område område) {
		this.nr = nr;
		this.område = område;
	}

	public static double getTimePris() {
		return standardTimePris;
	}

	public static void setTimePris(double standardTimePris) {
		Plads.standardTimePris = standardTimePris;
	}

	/**
	 * VIP-Område er 25% dyrere. Børneområde er 20% billigere. Turneringsområde er
	 * 10% dyrere.
	 * 
	 * @param timer
	 * @return
	 */
	public double pris(int timer) {
		double pris = standardTimePris * timer;
		if (område == Område.VIP) {
			pris *= 1.25;
		} else if (område == Område.BØRNE) {
			pris *= 0.80;
		} else if (område == Område.TURNERING) {
			pris *= 1.10;
		}
		return pris;
	}

	public int getNr() {
		return nr;
	}

	public void setNr(int nr) {
		this.nr = nr;
	}

	public Område getOmråde() {
		return område;
	}

	public void setOmråde(Område område) {
		this.område = område;
	}

	public void addReservation(Reservation reservation) {
		if (!reservationer.contains(reservation)) {
			reservationer.add(reservation);
			reservation.addPlads(this);
		}
	}

	public void removeReservationer(Reservation reservation) {
		if (reservationer.contains(reservation)) {
			reservationer.remove(reservation);
			reservation.removePlads(this);
		}
	}

	public ArrayList<Reservation> getReservationer() {
		return reservationer;
	}
}
