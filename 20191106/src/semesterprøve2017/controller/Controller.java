package semesterprøve2017.controller;

import java.time.LocalDateTime;
import java.util.ArrayList;

import semesterprøve2017.Område;
import semesterprøve2017.Plads;
import semesterprøve2017.Reservation;
import semesterprøve2017.storage.Storage;

public class Controller {

	// -------------------------------------------------------------------------
	// Pladser
	public static ArrayList<Plads> getPladser() {
		return Storage.getPladser();
	}

	public static void createPlads(int nr, Område område) {
		Storage.addPlads(new Plads(nr, område));
	}

	// -------------------------------------------------------------------------
	// Reservationer
	public static ArrayList<Reservation> getReservationer() {
		return Storage.getReservationer();
	}

	public static void createReservation(LocalDateTime start, LocalDateTime slut, Plads plads) {
		Reservation reservation = new Reservation(start, slut, plads);
		Storage.addReservation(new Reservation(start, slut, plads));
	}

	public static void createReservation(LocalDateTime start, LocalDateTime slut, ArrayList<Plads> pladser) {
		Storage.addReservation(new Reservation(start, slut, pladser));
	}
	// -------------------------------------------------------------------------
	/*
	public static initController() {
	
		/*
			Pladser: 
			nr. 1   turneringsområde 
			nr. 2  	turneringsområde 
			nr. 3 	standardområde 
			nr. 4  	standardområde 
			nr. 5 	børneområde 
			nr. 6 	VIPområde 
			Arrangementer: 
			- ”Dota 2 tournament”  offentligt arrangement 
			- ”CS GO tournament” 	privat arrangement 
		
			Reservationer: 
			2019-08-12 kl. 20:00 til 2019-08-12 kl. 23:00.  
			Plads nr. 1 og 2 - turneringsområdet 
			Tilhører arrangement: "Dota 2 tournament" 
			 
			 
			2019-08-13 kl. 19:00 til 2019-08-14 kl. 06:00 
			Plads nr. 3 og 4 - standardområdet 
			2019-08-14 kl. 19:00 til 2019-08-15 kl. 06:00 
		
	}
		 */
}
