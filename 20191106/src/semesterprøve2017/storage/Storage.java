package semesterprøve2017.storage;

import java.util.ArrayList;

import semesterprøve2017.Arrangement;
import semesterprøve2017.Plads;
import semesterprøve2017.Reservation;

public class Storage {
	private static ArrayList<Plads> pladser = new ArrayList<>();
	private static ArrayList<Reservation> reservationer = new ArrayList<>();
	private static ArrayList<Arrangement> arrangementer = new ArrayList<>();

	// -------------------------------------------------------------------------
	// Pladser
	public static ArrayList<Plads> getPladser() {
		return new ArrayList<Plads>(pladser);
	}

	public static void addPlads(Plads plads) {
		pladser.add(plads);
	}

	public static void removeCompany(Plads plads) {
		pladser.remove(plads);
	}

	// -------------------------------------------------------------------------
	// Reservationer
	public static ArrayList<Reservation> getReservationer() {
		return new ArrayList<Reservation>(reservationer);
	}

	public static void addReservation(Reservation reservation) {
		reservationer.add(reservation);
	}

	public static void removeCompany(Reservation reservation) {
		reservationer.remove(reservation);
	}

	// -------------------------------------------------------------------------
	// Arrangementer
	public static ArrayList<Arrangement> getArrangementer() {
		return new ArrayList<Arrangement>(arrangementer);
	}

	public static void addArrangement(Arrangement arrangement) {
		arrangementer.add(arrangement);
	}

	public static void removeCompany(Arrangement arrangement) {
		arrangementer.remove(arrangement);
	}

}
