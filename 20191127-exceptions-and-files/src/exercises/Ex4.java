package exercises;

import java.io.File;
import java.io.PrintWriter;
import java.util.Scanner;

public class Ex4 {
	/*
	 * 
	 Denne opgave går ud på at lave en generel fletning af to filer indeholdende heltal (int).
		a) Programmér en klasse med en main() metode.
		b) Lav to tekstfiler der indeholder int. Heltallene skal komme i stigende orden, indeholder
			ikke dubletter og begge filer skal som det sidste tal have:
			2147483647 (= Integer.MAX_VALUE)
		c) Programmer følgende metode i klassen som en generel fletning.
	 */

	public static void main(String[] args) {

	}

	/**
	 * Laver en sorteret fil i fileNameNy, der indeholder alle heltal, som de to
	 * filer har til fælles . Krav: filename1 og filename2 er navne på to sorterede
	 * filer.
	 */
	public static void findfællesTal(String fileName1, String fileName2, String fileNameNy) {
		File file1 = new File(fileName1);
		File file2 = new File(fileName2);

		try (Scanner sFile1 = new Scanner(file1);
				Scanner sFile2 = new Scanner(file2);
				PrintWriter result = new PrintWriter(fileNameNy)) {

			int i1 = sFile1.nextInt();
			int i2 = sFile1.nextInt();

			while (sFile1.hasNext() && sFile2.hasNext()) {
				if (i1 == i2) {

				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
