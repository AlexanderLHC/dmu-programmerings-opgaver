package exercises;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Opgave1 {
	/*
	 Lav med en fil med en række heltal, f.eks.
		34 -20 0 200 177 285
	(med præcis ét tal på hver linie).
	Lav en applikation, der i main-metoden læser denne fil og udskriver det dobbelte af tallene i
	consolvinduet, dvs.
		68 -40 0 400 354 570
	(på en eller flere linier).
	 */

	public static void main(String[] args) {
		File file = new File("ex1.txt");

		try {
			printEachLineDoubled(file);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	/*
	 * precondition: the file only contains valid integers
	 */
	public static void printEachLineDoubled(File inputFile) throws FileNotFoundException {
		Scanner file = new Scanner(inputFile);
		while (file.hasNext()) {
			int number = Integer.parseInt(file.next());
			System.out.printf("int %s*2=%s%n", number, number * 2);
		}
		file.close();
	}
}
