package exercises;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Opgave4 {
	/*
	 Opgave 4
	Lav en applikation der i main-metoden indlæser en række tal fra tastaturet (sluttende med -1),
	de indlæste tal skrives til en tekstfil. Tallet -1 skal ikke skrives i filen. De øvrige tal skal være positive.
	Tallene skal skrives i filen efterhånden, som de indlæses (de skal altså ikke gemmes i en ArrayList).
	 */

	public static void main(String[] args) throws FileNotFoundException {
		File file = new File("ex4.txt");
		boolean finished = false;
		Scanner console = new Scanner(System.in);
		PrintWriter out = new PrintWriter(file);

		while (!finished) {
			System.out.println("Write integer: ");
			int number = console.nextInt();
			console.nextLine(); // num num new line
			if (number == -1) {
				finished = true;
			} else {
				out.println(number);
			}
		}

		out.close();
		Scanner in = new Scanner(file);
		System.out.println("Printing resulting file:");
		while (in.hasNext()) {
			System.out.println(in.next());
		}
		in.close();
	}
}
