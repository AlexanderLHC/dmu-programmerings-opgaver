package exercises;

import java.io.File;
import java.io.PrintWriter;
import java.util.Scanner;

public class Ex3 {
	/*
	 * Denne opgave går ud på at lave en total fletning af to filer indeholdende heltal (int).
		a) Programmer en klasse med en main() metode.
		b) Lav to tekstfiler der indeholder int. Heltallene skal komme i stigende orden og begge filer
			skal som det sidste tal have: 2147483647 (= Integer.MAX_VALUE)
		c) Programmer følgende metode i klassen som en totalfletning.
	 */

	public static void main(String[] args) {
		fletAlleHeltal("opg3-file1.txt", "opg3-file2.txt", "opg3-file-result.txt");
	}

	/**
	 * Laver en sorteret fil i fileNameNy, der indeholder alle heltal fra fileName1
	 * og fileName2 (MAX_VALUE skal ikke i resultatet). Krav: filename1 og filename2
	 * er navne på to sorterede filer.
	 */
	public static void fletAlleHeltal(String fileName1, String fileName2, String fileNameNy) {
		File file1 = new File(fileName1);
		File file2 = new File(fileName2);

		try (Scanner sFile1 = new Scanner(file1);
				Scanner sFile2 = new Scanner(file2);
				PrintWriter result = new PrintWriter(fileNameNy)) {

			int i1 = Integer.parseInt(sFile1.nextLine());
			int i2 = Integer.parseInt(sFile2.nextLine());

			while (sFile1.hasNext() && sFile2.hasNext()) {

				if (i1 <= i2) {
					result.println(i1);
					i1 = Integer.parseInt(sFile1.nextLine());
				} else {
					result.println(i2);
					i2 = Integer.parseInt(sFile2.nextLine());
				}
			}

			while (sFile1.hasNext()) {
				result.println(i1);
				i1 = Integer.parseInt(sFile1.nextLine());
			}
			while (sFile2.hasNext()) {
				result.println(i1);
				i2 = Integer.parseInt(sFile2.nextLine());
			}

			result.close();
		} catch (Exception e) {
			System.out.println("ehhh");
			e.printStackTrace();
		}
	}
}
