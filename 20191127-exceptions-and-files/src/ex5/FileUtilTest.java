package ex5;

import java.io.IOException;

public class FileUtilTest {

	public static void testFileUtils(String filename) {
		System.out.printf("%n---------------%n");
		System.out.printf("Tests for %s:%n", filename);
		System.out.printf(" - highest number  =%10s %n", FileUtil.max(filename));
		System.out.printf(" - smallest number =%10s %n", FileUtil.min(filename));
		try {
			System.out.printf(" - average number  =%10.10s %n", FileUtil.average(filename));
		} catch (IOException e) {
			e.printStackTrace();
		}

		System.out.printf("---------------%n%n");
	}

}
