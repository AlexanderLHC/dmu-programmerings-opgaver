package exercise2;

import java.util.ArrayList;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class Ex2App extends Application {
	/*
	 * Exercise 2 Make a program with two windows according to the description
	 * below.
	 */

	private PersonInputWindow piw;
	private ObservableList<Person> olPeople;

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		GridPane pane = new GridPane();
		Scene scene = new Scene(pane, 300, 600);

		primaryStage.setTitle("Exercise 2");
		primaryStage.setScene(scene);
		initContent(pane);

		primaryStage.show();

		piw = new PersonInputWindow(primaryStage);
	}

	private void initContent(GridPane pane) {
		Label lblPersons = new Label("People");

		ArrayList<Person> people = new ArrayList<Person>(initPeople());

		olPeople = FXCollections.observableArrayList(people);

		ListView<Person> lwPeople = new ListView<Person>();
		lwPeople.setItems(olPeople);

		Button btnAddPerson = new Button("Add Person");
		btnAddPerson.setOnAction(e -> addPerson());

		pane.add(lblPersons, 0, 0);
		pane.add(lwPeople, 0, 1, 2, 2);
		pane.add(btnAddPerson, 0, 3);

	}

	private void addPerson() {
		piw.showAndWait();
		if (piw.getPerson() != null) {
			Person person = piw.getPerson();
			olPeople.add(person);
		}
	}

	private ArrayList<Person> initPeople() {
		ArrayList<Person> people = new ArrayList<Person>();

		people.add(new Person("Santa Claus", 99));
		people.add(new Person("Little Helper", 12));
		people.add(new Person("Mamma", 120));

		return people;
	}
}
