/**
 * 
 */
package ex2;

/**
 * @author alexander
 *
 */
public class KontoApp {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Konto konto = new Konto("Ung", 123114);
		konto.indsætBeløb(100);
		konto.indsætBeløb(20);
		konto.indsætBeløb(1000);
		konto.indsætBeløb(-100);
		System.out.println(konto.getSaldo());
	}

}
