package uo;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class UO4 extends Application {

	public static void main(String[] args) {
		Application.launch(args);
	}

	@Override
	public void start(Stage stage) {
		Pane root = this.initContent();
		Scene scene = new Scene(root);

		stage.setTitle("Shapes");
		stage.setScene(scene);
		stage.show();
	}

	private Pane initContent() {
		Pane pane = new Pane();
		pane.setPrefSize(400, 400);
		this.drawShapes(pane);
		return pane;
	}

	// ------------------------------------------------------------------------

	private void drawShapes(Pane pane) {
		Circle ring1 = new Circle(200, 200, 100);
		Circle ring2 = new Circle(200, 200, 70);
		Circle ring3 = new Circle(200, 200, 60);
		Circle ring4 = new Circle(200, 200, 15);

		ring1.setFill(Color.WHITESMOKE);
		ring2.setFill(Color.BLACK);
		ring3.setFill(Color.WHITESMOKE);
		ring4.setFill(Color.BLACK);
		int strokeWidth = 15;
		ring1.setStrokeWidth(strokeWidth);
		ring1.setStroke(Color.BLACK);
		ring2.setStroke(Color.WHITESMOKE);
		ring2.setStrokeWidth(strokeWidth);
		ring3.setStroke(Color.BLACK);
		ring3.setStrokeWidth(strokeWidth);

		// add shapes
		pane.getChildren().addAll(ring1, ring2, ring3, ring4);
	}

}
