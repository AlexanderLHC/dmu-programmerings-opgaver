package uo;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class UO2 extends Application {

	public static void main(String[] args) {
		Application.launch(args);
	}

	@Override
	public void start(Stage stage) {
		Pane root = this.initContent();
		Scene scene = new Scene(root);

		stage.setTitle("Shapes");
		stage.setScene(scene);
		stage.show();
	}

	private Pane initContent() {
		Pane pane = new Pane();
		pane.setPrefSize(400, 400);
		this.drawShapes(pane);
		return pane;
	}

	// ------------------------------------------------------------------------

	private void drawShapes(Pane pane) {
		Rectangle rectangle = new Rectangle(200, 200, 400, 400);
		rectangle.setFill(Color.BLUE);
		rectangle.setStroke(Color.RED);

		Text textName = new Text("Alexander");
		textName.setX(300);
		textName.setY(300);
		textName.setFill(Color.DARKGREEN);
		textName.setStrokeWidth(2);

		// add shapes
		pane.getChildren().addAll(rectangle, textName);
	}

}
