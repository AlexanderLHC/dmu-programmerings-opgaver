package uo;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class UO3 extends Application {

	public static void main(String[] args) {
		Application.launch(args);
	}

	@Override
	public void start(Stage stage) {
		Pane root = this.initContent();
		Scene scene = new Scene(root);

		stage.setTitle("Shapes");
		stage.setScene(scene);
		stage.show();
	}

	private Pane initContent() {
		Pane pane = new Pane();
		pane.setPrefSize(400, 400);
		this.drawShapes(pane);
		return pane;
	}

	// ------------------------------------------------------------------------

	private void drawShapes(Pane pane) {
		Circle face = new Circle(200, 200, 200);
		Circle eyeLeft = new Circle(110, 120, 30);
		Circle eyeRight = new Circle(290, 120, 30);
		Line mouth = new Line(120, 260, 280, 260);
		Line mouthAngleLeft = new Line(280, 260, 300, 240);
		Line mouthAngleRight = new Line(120, 260, 100, 240);

		face.setFill(Color.BEIGE);
		eyeLeft.setFill(Color.BLUE);
		eyeRight.setFill(Color.BLUE);
		mouth.setStroke(Color.DARKRED);
		mouthAngleLeft.setStroke(Color.DARKRED);
		mouthAngleRight.setStroke(Color.DARKRED);

		// add shapes
		pane.getChildren().addAll(face, eyeLeft, eyeRight, mouth, mouthAngleLeft, mouthAngleRight);

	}

}
