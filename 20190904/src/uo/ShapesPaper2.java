package uo;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

public class ShapesPaper2 extends Application {

	public static void main(String[] args) {
		Application.launch(args);
	}

	@Override
	public void start(Stage stage) {
		Pane root = this.initContent();
		Scene scene = new Scene(root);

		stage.setTitle("Shapes");
		stage.setScene(scene);
		stage.show();
	}

	private Pane initContent() {
		Pane pane = new Pane();
		pane.setPrefSize(400, 400);
		this.drawShapes(pane);
		return pane;
	}

	// ------------------------------------------------------------------------

	private void drawShapes(Pane pane) {
		int circleX = 70 + 100;
		int lineX = 32 + 100;
		int rectangleX = 200 + 100;
		Color shapeColor = Color.YELLOW;

		Circle circle = new Circle(circleX, 70, 30);
		circle.setFill(shapeColor);
		circle.setStroke(shapeColor);

		Line line = new Line(lineX, 32, 320, 320);
		line.setStroke(shapeColor);

		Rectangle rectangle = new Rectangle(rectangleX, 200, 400, 400);
		rectangle.setFill(shapeColor);
		rectangle.setStroke(shapeColor);

		// add shapes
		pane.getChildren().addAll(circle, line, rectangle);
	}

}
