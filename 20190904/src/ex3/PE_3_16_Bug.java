/**
 * 
 */
package ex3;

/**
 * @author alexander
 *
 */
public class PE_3_16_Bug {
	private int position;
	private int direction = 1;

	/**
	 * @param args
	 */
	/*
	 *  E3.16
	 *  Write a class Bug that models a bug moving along a horizontal line. 
	 *  The bug moves either to the right or left. Initially, the bug moves to the right,
	 *   but it can turn to change its direction. In each move, 
	 *   its position changes by one unit in the current direction. 
	 *   Provide a constructor
	 *  
		public Bug(int initialPosition) 
		and methods
	
		public void turn()
		public void move()
		public int getPosition() 
		Sample usage:
	
		Bug bugsy = new Bug(10);
		bugsy.move(); // Now the position is 11
		bugsy.turn(); 
		bugsy.move(); // Now the position is 10 
		Your BugTester shou
	 */

	public PE_3_16_Bug(int position) {
		this.position = position;
	}

	public int getPosition() {
		return position;
	}

	public void turn() {
		direction = direction * (-1);
	}

	public void move() {
		position += direction;
	}

}
