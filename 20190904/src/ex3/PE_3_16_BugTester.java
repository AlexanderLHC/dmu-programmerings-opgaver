/**
 * 
 */
package ex3;

/**
 * @author alexander
 *
 */
public class PE_3_16_BugTester {
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		PE_3_16_Bug bug = new PE_3_16_Bug(0);
		System.out.println("Initial position: " + bug.getPosition());
		bug.move();
		System.out.println("Move: " + bug.getPosition());
		bug.turn();
		System.out.println("Turn: " + bug.getPosition());
		bug.turn();
		System.out.println("Turn: " + bug.getPosition());
		bug.move();
		System.out.println("Move: " + bug.getPosition());
		bug.move();
		System.out.println("Move: " + bug.getPosition());
		bug.turn();
		System.out.println("Turn: " + bug.getPosition());
		bug.move();
		System.out.println("Move: " + bug.getPosition());
		bug.move();
		System.out.println("Move: " + bug.getPosition());
		bug.move();
		System.out.println("Move: " + bug.getPosition());
		bug.move();
		System.out.println("Move: " + bug.getPosition());
	}

}
