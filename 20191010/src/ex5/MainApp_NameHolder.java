package ex5;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class MainApp_NameHolder extends Application {

	public static void main(String[] args) {
		Application.launch(args);
	}

	@Override
	public void start(Stage stage) {
		stage.setTitle("Ex 5: Name holder");
		GridPane pane = new GridPane();
		this.initContent(pane);
		Scene scene = new Scene(pane);
		stage.setScene(scene);
		stage.show();
	}

	// -------------------------------------------------------------------------

	private final TextField txfNameInput = new TextField();
	private final TextArea txaNames = new TextArea();

	private void initContent(GridPane pane) {
		// show or hide grid lines
		pane.setGridLinesVisible(false);

		// set padding of the pane
		pane.setPadding(new Insets(20));
		// set horizontal gap between components
		pane.setHgap(10);
		// set vertical gap between components
		pane.setVgap(10);

		// Label
		Label lblFutureValue = new Label("Name:");
		pane.add(lblFutureValue, 0, 0);
		// Fields
		pane.add(txfNameInput, 0, 1, 3, 1);
		pane.add(txaNames, 0, 3, 3, 1);
		txaNames.setEditable(false);

		// Buttons
		Button btnAddName = new Button("Add");
		pane.add(btnAddName, 1, 2);
		btnAddName.setOnAction(event -> this.addName());
		Button btnClear = new Button("Clear All");
		pane.add(btnClear, 2, 2);
		btnClear.setOnAction(event -> this.clearNames());

	}

	// -------------------------------------------------------------------------
	private void addName() {
		StringBuilder sb = new StringBuilder();
		sb.append(txaNames.getText());
		sb.append(txfNameInput.getText() + "\n");
		txaNames.setText(sb.toString());
		txfNameInput.setText("");
	}

	private void clearNames() {
		txaNames.setText("");
	}
}
