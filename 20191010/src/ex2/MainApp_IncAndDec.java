package ex2;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class MainApp_IncAndDec extends Application {

	public static void main(String[] args) {
		Application.launch(args);
	}

	@Override
	public void start(Stage stage) {
		stage.setTitle("Ex 2: Incrementer and Decrementer");
		GridPane pane = new GridPane();
		this.initContent(pane);
		Scene scene = new Scene(pane);
		stage.setScene(scene);
		stage.show();
	}

	// -------------------------------------------------------------------------

	private final TextField txfCounter = new TextField();

	private void initContent(GridPane pane) {
		// show or hide grid lines
		pane.setGridLinesVisible(false);

		// set padding of the pane
		pane.setPadding(new Insets(20));
		// set horizontal gap between components
		pane.setHgap(10);
		// set vertical gap between components
		pane.setVgap(10);

		// Label
		Label lblCounter = new Label("Counter:");
		pane.add(lblCounter, 0, 0, 1, 2);

		// Fields
		pane.add(txfCounter, 1, 0, 1, 2);
		txfCounter.setText(0 + "");

		// Buttons
		// - increment
		Button btnInc = new Button("+");
		pane.add(btnInc, 2, 0);
		btnInc.setOnAction(event -> this.countIncrement());
		// - decrement
		Button btnDec = new Button("-");
		pane.add(btnDec, 2, 1);
		btnDec.setOnAction(event -> this.countDecrement());

	}

	// -------------------------------------------------------------------------

	private void countIncrement() {
		txfCounter.setText((Integer.parseInt(txfCounter.getText()) + 1) + "");
	}

	private void countDecrement() {
		txfCounter.setText((Integer.parseInt(txfCounter.getText()) - 1) + "");
	}

}
