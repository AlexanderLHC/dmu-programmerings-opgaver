package ex3;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class MainApp_InvestCalc extends Application {

	public static void main(String[] args) {
		Application.launch(args);
	}

	@Override
	public void start(Stage stage) {
		stage.setTitle("Ex 3: Investor");
		GridPane pane = new GridPane();
		this.initContent(pane);
		Scene scene = new Scene(pane);
		stage.setScene(scene);
		stage.show();
	}

	// -------------------------------------------------------------------------

	private final TextField txfInvestment = new TextField();
	private final TextField txfYear = new TextField();
	private final TextField txfInterest = new TextField();
	private final TextField txfFutureValue = new TextField();

	private void initContent(GridPane pane) {
		// show or hide grid lines
		pane.setGridLinesVisible(false);

		// set padding of the pane
		pane.setPadding(new Insets(20));
		// set horizontal gap between components
		pane.setHgap(10);
		// set vertical gap between components
		pane.setVgap(10);

		// Label
		Label lblInvestment = new Label("Investment value:");
		pane.add(lblInvestment, 0, 0);
		Label lblYear = new Label("Duration (years):");
		pane.add(lblYear, 0, 1);
		Label lblInterest = new Label("Interest (%):");
		pane.add(lblInterest, 0, 2);
		Label lblFutureValue = new Label("Future value:");
		pane.add(lblFutureValue, 0, 4);
		// Fields
		pane.add(txfInvestment, 1, 0);
		pane.add(txfYear, 1, 1);
		pane.add(txfInterest, 1, 2);
		pane.add(txfFutureValue, 1, 4);

		// Buttons
		Button btnCalc = new Button("Calculate");
		pane.add(btnCalc, 1, 3);
		GridPane.setMargin(btnCalc, new Insets(0, 0, 0, 40));
		btnCalc.setOnAction(event -> this.calcFutureValue());

	}

	// -------------------------------------------------------------------------
	private void calcFutureValue() {
		int investment = Integer.parseInt(txfInvestment.getText());
		int year = Integer.parseInt(txfYear.getText());
		double interest = Double.parseDouble(txfInterest.getText()) / 100;

		double futureValue = investment * (1 + (interest * year));
		txfFutureValue.setText("" + futureValue);
	}

}
