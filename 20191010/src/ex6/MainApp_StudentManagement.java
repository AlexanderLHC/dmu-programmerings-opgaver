package ex6;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class MainApp_StudentManagement extends Application {

	public static void main(String[] args) {
		Application.launch(args);
	}

	@Override
	public void start(Stage stage) {
		stage.setTitle("Ex 6: Student Management Tool");
		GridPane pane = new GridPane();
		this.initContent(pane);
		Scene scene = new Scene(pane);
		stage.setScene(scene);
		stage.show();
	}

	// -------------------------------------------------------------------------

	private final TextArea txaStudentInfo = new TextArea();
	private final TextField txfName = new TextField();
	private final TextField txfAge = new TextField();
	private final CheckBox chkIsActive = new CheckBox();
	private Button btnCreate = new Button();
	private Button btnUpdate = new Button();
	private Button btnDelete = new Button();
	private Student student = new Student();

	private void initContent(GridPane pane) {
		// show or hide grid lines
		pane.setGridLinesVisible(false);

		// === Style
		pane.setPadding(new Insets(20));
		pane.setHgap(10);
		pane.setVgap(10);

		// === Label
		Label lblStudentInfo = new Label("Student Info:");
		Label lblName = new Label("Name:");
		Label lblAge = new Label("Age:");
		Label lblActive = new Label("Active:");

		// === Fields
		txaStudentInfo.setEditable(false);

		// === Buttons
		btnCreate.setText("Create");
		btnCreate.setOnAction(event -> this.fillStudent());
		btnUpdate.setText("Update");
		btnUpdate.setDisable(true);
		btnUpdate.setOnAction(event -> this.fillStudent());
		btnDelete.setText("Delete");
		btnDelete.setDisable(true);
		btnDelete.setOnAction(event -> this.deleteStudent());
		Button btnBirthday = new Button("+");
		btnBirthday.setOnAction(event -> this.studentBirthday());
		Button btnSetAge = new Button("Set age");
		btnSetAge.setOnAction(event -> this.setAge());

		// === Pane
		int row = 0;
		// Row 0
		pane.add(lblStudentInfo, 0, row);
		// Row 1 + 2
		row++;
		pane.add(txaStudentInfo, 0, row, 3, 2);
		// Row 3
		row = 3;
		pane.add(lblName, 0, row);
		pane.add(txfName, 1, row, 2, 1);
		// Row 4
		row++;
		pane.add(lblAge, 0, row);
		pane.add(txfAge, 1, row, 1, 1);
		pane.add(btnBirthday, 2, row);
		pane.add(btnSetAge, 2, row);
		GridPane.setMargin(btnSetAge, new Insets(0, 0, 0, 25));
		// Row 5
		row++;
		pane.add(lblActive, 0, row);
		pane.add(chkIsActive, 1, row, 2, 1);
		// Row 6
		row++;
		pane.add(btnCreate, 0, row);
		pane.add(btnUpdate, 1, row);
		pane.add(btnDelete, 2, row);
		GridPane.setMargin(btnCreate, new Insets(0, 0, 0, 25));
	}

	// -------------------------------------------------------------------------

	private void fillStudent() {

		student.setName(txfName.getText());
		student.setAge(Integer.parseInt(txfAge.getText()));
		student.setActive(Boolean.parseBoolean(chkIsActive.getText()));

		studentToGUI(student);
		this.toggleButtons();
	}

	private void studentToGUI(Student student) {
		StringBuilder sb = new StringBuilder();
		sb.append("Name: " + student.getName() + "\n");
		sb.append("Age: " + student.getAge() + "\n");
		sb.append("Active: " + student.getIsActive() + "\n");

		txaStudentInfo.setText(sb.toString());
	}

	private void toggleButtons() {
		if (!isFilled()) {
			btnCreate.setDisable(false);
			btnDelete.setDisable(true);
			btnUpdate.setDisable(true);
		} else {
			btnCreate.setDisable(true);
			btnDelete.setDisable(false);
			btnUpdate.setDisable(false);
		}
	}

	private void deleteStudent() {
		txaStudentInfo.setText("");
		this.toggleButtons();
	}

	private void studentBirthday() {
		student.incAge();
		this.studentToGUI(student);
		txfAge.setText("" + student.getAge());
	}

	private void setAge() {
		student.setAge(Integer.parseInt(txfAge.getText()));
		this.studentToGUI(student);
	}

	private boolean isFilled() {
		System.out.println(txaStudentInfo.getText().length());
		if (txaStudentInfo.getText().length() > 1) {
			return true;
		} else {
			return false;
		}
	}
}
