package ex6;

/*
   
At top there is a text area (non-editable) that is going to show all the information about a 
student. Then there are 3 input fields: 2 text fields used to type in name and age, and a check 
box to set the activity level of the student.
The Create button creates a Student object from the values in the input fields, shows the info 
about the student in the text area, and clears the input fields (and disables/enables buttons).
Make a Student field in the MainApp class to remember the student in.

The Update button updates the Student object with the values in the input fields, and shows 
the updated info in the text area..
The Delete button deletes the Student object and clears the text area and the input fields.
 
The Inc button updates the value in the Age text field with 1.
The Reset button updates the input fields with values from the Student object.
Remember to disable/enable buttons to help the user of the program to use the program in the 
 */

public class Student {
	private String name;
	private int age;
	private boolean isActive;

	public Student() {
		name = "";
		age = 0;
		isActive = false;
	}

	public void incAge() {
		age++;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	public String getIsActive() {
		if (isActive) {
			return "Yes";
		} else {
			return "No";
		}
	}
}
