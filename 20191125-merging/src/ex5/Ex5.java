package ex5;

public class Ex5 {

	/*
	 * Exercise 5
	Program a Card class that models a playing card. Use two enumeration types: 
	Suit with values CLUBS, DIAMONDS, HEARTS og SPADES, and 
	Rank with values TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, JACK, QUEEN, KING, ACE.
	
	Add to the Card class a constructor public Card(Suit s, Rank r) that constructs a Card object.
	The Card class must implement Comparable<Card>. Cards are ordered on value (2 < 3 < ...
	< ace) and then on suit (clubs < diamonds < hearts < spades).
	Add a toString() method to the Card class (nust return suit and value)
	Hint: Use that an enumeration type E implements Comparable<E> and has a toString()
	method.
	Create a Hand class. The class models a hand of cards (from only one deck of cards). Add the
	following methods: addCard(Card card): boolean and removeCard(Card card): boolean (the
	addCard method return true unless addCard tries to add a card already in the hand; the
	removeCard method returns true unless removeCard tries to remove a card not in the hand).
	Add a toString() method that returns the cards in ascending order.
	Test your classes in a main() method.
	 */
	public static void main(String[] args) {
		Card c1 = new Card(Suit.HEARTS, Value.TWO);
		Card c2 = new Card(Suit.DIAMONDS, Value.EIGHT);
		Hand hand = new Hand();

		System.out.printf("Adding %s to hand. Succesful: %s. Hand contains: %s.%n", c1, hand.addCard(c1), hand);
		System.out.printf("Adding %s to hand. Succesful: %s. Hand contains: %s.%n", c1, hand.addCard(c1), hand);
		System.out.printf("Adding %s to hand. Succesful: %s. Hand contains: %s.%n", c2, hand.addCard(c2), hand);
		System.out.printf("Removing %s from hand. Succesful: %s. Hand contains: %s.%n", c2, hand.removeCard(c2), hand);
		System.out.printf("Removing %s from hand. Succesful: %s. Hand contains: %s.%n", c2, hand.removeCard(c2), hand);
	}
}
