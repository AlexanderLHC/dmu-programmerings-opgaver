package exercises;

import java.util.ArrayList;
import java.util.Collections;

public class Ex1 {
	/*
	Exercise 1 
	In this exercise you are going to merge two ArrayLists of Customer objects. 
	a) Program the following method (use a total merging) in a class with a main() method : 
	 Returns a sorted list containing all customers 
	 from the lists l1 and l2. 
	 Requires: l1 and l2 are sorted. 
	 public static ArrayList<Customer> mergeAllCustomers ( 
	 
	ArrayList<Customer> l1, ArrayList<Customer> l2) {...} 
	 
	b) Add to the package a Customer class that implements Comparable<Customer>. Customers 
	are ordered by last name and then first name. 
	c) Test the method mergeAllCustomers() in the main() method (remember that the lists used 
	 */
	public static void main(String[] args) {
		ArrayList<Customer> customers1 = new ArrayList<>();
		ArrayList<Customer> customers2 = new ArrayList<>();

		for (int i = 0; i < 5; i++) {
			customers1.add(new Customer());
			customers2.add(new Customer());
			if (i < 2) {
				customers1.get(i).setFirstname("Peter");
			}
		}
		System.out.printf("List of customers with first and lastname: %n");
		System.out.printf("Customer list1 unsorted: %s %n", customers1);
		System.out.printf("Customer list2 unsorted: %s %n", customers2);

		// a)
		// b)
		System.out.printf("a+b) customers lists are sorted");
		Collections.sort(customers1);
		Collections.sort(customers2);
		System.out.printf("Customer list1 sorted: %s %n", customers1);
		System.out.printf("Customer list2 sorted: %s %n", customers2);
		// c)
		System.out.printf("c) Merging the sorted lists:%n");
		ArrayList<Customer> customersMerged = mergeAll(customers1, customers2);
		System.out.printf("%s%n", customersMerged);

	}

	public static ArrayList<Customer> mergeAll(ArrayList<Customer> customers1, ArrayList<Customer> customers2) {
		ArrayList<Customer> customersMerged = new ArrayList<Customer>();
		int i1 = 0;
		int i2 = 0;

		while (i1 < customers1.size() && i2 < customers2.size()) {
			if (customers1.get(i1).compareTo((customers2.get(i2))) <= 0) {
				// s1's første tal er mindst
				customersMerged.add(customers1.get(i1));
				i1++;
			} else { // s2's første tal er mindst
				customersMerged.add(customers2.get(i2));
				i2++;
			}
		}
		// tøm den liste der ikke er tom
		while (i1 < customers1.size()) {
			customersMerged.add(customers1.get(i1));
			i1++;
		}
		while (i2 < customers2.size()) {
			customersMerged.add(customers2.get(i2));
			i2++;
		}
		return customersMerged;
	}

}
