# Datamatiker 1 Semester: 19U

Abbriviations | Excercise type
--------------|
pe            | Practice Exercises
pp            | Programming Projects
re            | Review Exercises
uo            | Udleverede opgaver


## Big Java opgaver

### Chapter 1

![pe](assignments/cp1_practice_exercises.png)

![Programming projects](assignments/cp1_programming_projects.png)

![Review Excerises](/home/alexander/dev/eaaa/assignments/cp1_review_excerises.png)

## Udleverede opgaver

### Shapes (paper)

![Shapes paper](assignments/20190904_shapes_paper_exercises.png)

### Udleveret opgave

![Udleveret opgave](assignments/20190904_uo.png)
