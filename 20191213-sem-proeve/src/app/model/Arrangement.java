package app.model;

import java.time.LocalDate;
import java.time.LocalTime;

public class Arrangement {
	private String title;
	private LocalDate date;
	private LocalTime startTid;
	private LocalTime slutTid;
	private double pris;

	public Arrangement(String title, LocalDate date, LocalTime startTid, LocalTime slutTid, double pris) {
		this.title = title;
		this.date = date;
		this.startTid = startTid;
		this.slutTid = slutTid;
		this.pris = pris;
	}

	public String getTitle() {
		return title;
	}

	public LocalDate getDate() {
		return date;
	}

	public LocalTime getStartTid() {
		return startTid;
	}

	public LocalTime getSlutTid() {
		return slutTid;
	}

	public double getPris() {
		return pris;
	}

	@Override
	public String toString() {
		return "Arrangement [title=" + title + ", date=" + date + ", startTid=" + startTid + ", slutTid=" + slutTid
				+ ", pris=" + pris + "]";
	}

}
