package app.model;

import java.util.ArrayList;

public class Hold {

	private String betegnelse;
	private String holdleder;
	private Uddannelse uddannelse;
	private ArrayList<Tutor> tutorer = new ArrayList<Tutor>();

	public Hold(String betegnelse, String holdleder, Uddannelse uddannelse) {
		this.betegnelse = betegnelse;
		this.holdleder = holdleder;
		this.uddannelse = uddannelse;
	}

	public String getBetegnelse() {
		return betegnelse;
	}

	public String getHoldleder() {
		return holdleder;
	}

	public Uddannelse getUddannelse() {
		return uddannelse;
	}

	public void addTutor(Tutor tutor) {
		if (!tutorer.contains(tutor)) {
			tutorer.add(tutor);
			tutor.setHold(this);
		}
	}

	public void removeTutor(Tutor tutor) {
		if (!tutorer.contains(tutor)) {
			tutorer.remove(tutor);
			tutor.setHold(null);
		}
	}

	public ArrayList<Tutor> getTutorer() {
		return tutorer;
	}

	public double arrangementsPris() {
		ArrayList<Arrangement> arrangementer = new ArrayList<Arrangement>();
		double sum = 0;
		for (Tutor t : tutorer) {
			for (Arrangement a : t.getArrangementer()) {
				if (!arrangementer.contains(a)) {
					arrangementer.add(a);
				}
			}
		}
		for (Arrangement a : arrangementer) {
			sum += a.getPris();
		}
		return sum;
	}
	
	public boolean harTidsoverlap(Arrangement a) {
		boolean overlap = false;
		int i = 0;
		int c = 0;
		while (!overlap && tutorer.size() < i) {
			Tutor tutor = tutorer.get(i);
			while (!overlap && tutor.getArrangementer().size() > c) {
				Arrangement tArr = tutor.getArrangementer().get(c);
				if (!tArr.getDate().equals(a.getDate()) || tArr.getSlutTid().isBefore(a.getStartTid()) || tArr.getStartTid().isAfter(a.getSlutTid())) {
					overlap = true;
				}
				c++;
			}
			i++;
		}
		return overlap;
		
//		boolean overlap = false;
//		for (Tutor tutor : tutorer) {
//			for (Arrangement tArr: tutor.getArrangementer()) {
//				if (!tArr.getDate().equals(a.getDate()) || tArr.getSlutTid().isBefore(a.getStartTid()) || tArr.getStartTid().isAfter(a.getSlutTid())) {
//					overlap = true;
//					break;
//				}
//			}
//		}
//		return overlap;
	}

	@Override
	public String toString() {
		return "Hold [betegnelse=" + betegnelse + "]";
	}

}
