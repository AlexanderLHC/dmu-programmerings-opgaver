package app.model;

import java.util.ArrayList;
import java.util.Collections;

public class Uddannelse {
	private String navn;
	private ArrayList<Hold> holdListe = new ArrayList<Hold>();

	public Uddannelse(String navn) {
		this.navn = navn;
	}

	public String getNavn() {
		return navn;
	}

	public Hold createHold(String betegnelse, String holdleder) {
		Hold hold = new Hold(betegnelse, holdleder, this);
		holdListe.add(hold);
		return hold;
	}

	public ArrayList<Hold> getHoldListe() {
		return holdListe;
	}

	public ArrayList<String> tutorOversigt() {
		ArrayList<String> oversigt = new ArrayList<String>();
		for (Hold hold : holdListe) {
			for (Tutor t : hold.getTutorer()) {
				oversigt.add(t.toString());
			}
		}

		Collections.sort(oversigt);
		return oversigt;
	}

	@Override
	public String toString() {
		return "Uddannelse [navn=" + navn + ", holdListe=" + holdListe + "]";
	}

}
