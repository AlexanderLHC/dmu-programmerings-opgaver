package ex4And5;

import java.util.ArrayList;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

public class MainApp extends Application {

	public static void main(String[] args) {
		Application.launch(args);
	}

	@Override
	public void start(Stage stage) {
		stage.setTitle("Ex 4 & 5: Add name with gender");
		GridPane pane = new GridPane();
		this.initContent(pane);
		Scene scene = new Scene(pane);
		stage.setScene(scene);
		stage.show();
	}

	// -------------------------------------------------------------------------

	// Field variables
	private ArrayList<Person> people = new ArrayList<Person>();
	private ListView<Person> lvwPeople = new ListView<Person>();
	private TextField txfName = new TextField();
	private final ToggleGroup grpGender = new ToggleGroup();

	private void initContent(GridPane pane) {
		// show or hide grid lines
		pane.setGridLinesVisible(false);

		// set padding of the pane
		pane.setPadding(new Insets(20));
		// set horizontal gap between components
		pane.setHgap(10);
		// set vertical gap between components
		pane.setVgap(10);

		// Label
		Label lblNames = new Label("Names: ");
		Label lblName = new Label("Name: ");
		// RadioBox
		HBox radioBox = new HBox();
		RadioButton rbBoys = new RadioButton();
		radioBox.getChildren().add(rbBoys);
		rbBoys.setText("Boys");
		rbBoys.setUserData(true);
		rbBoys.setToggleGroup(grpGender);
		rbBoys.setOnAction(event -> refreshList());

		RadioButton rbGirls = new RadioButton();
		radioBox.getChildren().add(rbGirls);
		rbGirls.setText("Girls");
		rbGirls.setUserData(false);
		rbGirls.setToggleGroup(grpGender);
		rbGirls.setOnAction(event -> refreshList());

		// Buttons
		Button btnAdd = new Button("Add");
		btnAdd.setOnAction(event -> addPerson());

		// Pane
		pane.add(radioBox, 0, 0);
		pane.add(lblNames, 0, 1);
		pane.add(lvwPeople, 1, 1);
		pane.add(lblName, 0, 2);
		pane.add(txfName, 1, 2);
		pane.add(btnAdd, 2, 2);

	}

	// -------------------------------------------------------------------------
	private void addPerson() {
		if (txfName.getText().length() > 0) {
			String name = txfName.getText();
			boolean isBoy = (boolean) grpGender.getSelectedToggle().getUserData();
			Person person = new Person(name, isBoy);
			people.add(person);
		} else {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Wups!");
			alert.setHeaderText("Forgot to add name");
			alert.setContentText("Try again");
			alert.show();
		}
		refreshList();
	}

	/*
	 * Could be more efficient to have separate gender lists.
	 * However wanted to test alternative solutions.
	 */
	private void refreshList() {
		boolean isBoy = (boolean) grpGender.getSelectedToggle().getUserData();

		ArrayList<Person> peopleToList = new ArrayList<>();
		if (isBoy) {
			for (Person p : people) {
				if (p.isBoy()) {
					peopleToList.add(p);
				}
			}
		} else { // if girl
			for (Person p : people) {
				if (!p.isBoy()) {
					peopleToList.add(p);
				}
			}
		}
		lvwPeople.getItems().clear();
		lvwPeople.getItems().setAll(peopleToList);
	}

}
