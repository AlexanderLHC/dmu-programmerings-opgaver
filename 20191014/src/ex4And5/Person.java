package ex4And5;

public class Person {
	private String name;
	private boolean isBoy;

	public Person(String name, boolean isBoy) {
		this.name = name;
		this.isBoy = isBoy;
	}

	public boolean isBoy() {
		return isBoy;
	}

	@Override
	public String toString() {
		return String.format("%s", name);
	}
}
