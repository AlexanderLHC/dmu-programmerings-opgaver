package ex3;

public class PersonEx3 {
	private String name;
	private String title;
	private boolean isSenior;

	public PersonEx3(String name, String title, boolean isSenior) {
		this.name = name;
		this.title = title;
		this.isSenior = isSenior;
	}

	@Override
	public String toString() {
		String s = title + " " + name;
		if (isSenior) {
			s = s + " (senior)";
		}
		return s;
	}
}