package ex3Swimmers;

import java.util.ArrayList;

/*
 * Opgave 2 
	her går ud på at vende navigeringen i opgave 1 om, så det nu er træningsplanen der kender 

	a. Implementer associeringen, givet at der kun er brug for at finde de tilknyttede svømmere ud 
		fra træningsplanen – ikke den anden vej! Hent inspiration i noten afsnit 2. 
	b. Lav en ny SwimmerApp klasse, som opretter to svømmere og en træningsplan og sætter 
		svømmerne som svømmere på planen. 
	c. Udvid SwimmingApp-klassen, så du ud fra træningsplanen finder de svømmere, som er 
		tilknyttet planen, og udskriver informationer om hver svømmer på skærmen.
	
 */

public class SwimmerTrainingTester {

	public static void main(String[] args) {
		TrainingPlan planA = new TrainingPlan('A', 16, 10);
		TrainingPlan planB = new TrainingPlan('B', 10, 6);
		TrainingPlan planC = new TrainingPlan('C', 8, 2);

		ArrayList<Double> lapTimes = new ArrayList<>();
		lapTimes.add(1.02);
		lapTimes.add(1.01);
		lapTimes.add(0.99);
		lapTimes.add(0.98);
		lapTimes.add(1.02);
		lapTimes.add(1.04);
		lapTimes.add(0.99);
		Swimmer s1 = new Swimmer("Jan", 1994, lapTimes, "AGF");

		lapTimes = new ArrayList<>();
		lapTimes.add(1.05);
		lapTimes.add(1.01);
		lapTimes.add(1.04);
		lapTimes.add(1.06);
		lapTimes.add(1.08);
		lapTimes.add(1.04);
		lapTimes.add(1.02);
		Swimmer s2 = new Swimmer("Bo", 1995, lapTimes, "Lyseng");

		lapTimes = new ArrayList<>();
		lapTimes.add(1.03);
		lapTimes.add(1.01);
		Swimmer s3 = new Swimmer("Mikkel", 1993, lapTimes, "AITA-Tranbjerg");

		System.out.println(planA);
		System.out.println(planB);

		System.out.println("Adding swimmers to training plans");
		s1.setTrainingPlan(planA);
		planA.addSwimmer(s2);
		s3.setTrainingPlan(planB);
		System.out.println(planA);
		System.out.println(planB);

	}

}
