package ex1Cars;

import java.util.ArrayList;

public class Car {
	private String license;
	private double pricePerDay;
	private int purchaseYear;
	private ArrayList<Rental> rentals = new ArrayList<>();

	public Car(String license, int year) {
		this.license = license;
		purchaseYear = year;
	}

	public void setDayPrice(double pricePerDay) {
		this.pricePerDay = pricePerDay;
	}

	public double getDayPrice() {
		return pricePerDay;
	}

	public String getLicense() {
		return license;
	}

	public int getPurchaseYear() {
		return purchaseYear;
	}

	/**
	 * PRE: rental period doesn't overlap with another for given car (double
	 * booking).
	 * 
	 * @param r
	 */
	public void addRental(Rental r) {
		if (!rentals.contains(r)) {
			rentals.add(r);
			r.addCar(this);
		}
	}

	public void removeRental(Rental r) {
		rentals.remove(r);
	}

	public Rental longestRentalPeriod() {
		Rental longestRentalPeriod = rentals.get(0);
		for (Rental r : rentals) {
			if (r.getDays() > longestRentalPeriod.getDays()) {
				longestRentalPeriod = r;
			}
		}
		return longestRentalPeriod;
	}
}
