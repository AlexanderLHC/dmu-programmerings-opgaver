package ex2Gifts;

public class GiftPersonTester {

	/*
	Opgave 2:
	a. Programmer de to klasser. 
	b. Implementer associeringen receives, idet der kun er behov for at finde ud af, hvilke gaver en 
		person modtager. Bemærk, at retningspilen ikke er tegnet på klassediagrammet, så tegn først 
		denne. Associeringerne er altså enkeltrettede. 
	c. Nu skal der tilføjes en metode, der beregner, den samlede værdi af de gaver en person 
		modtager. På hvilken klasse skal denne metode ligge?  
	d. Lav en afprøvningsklasse, hvor du oprette nogle gaver og personer, og dernæst lader personerne
		modtage disse gaver. Afprøv dernæst metoden fra delopgave c).
		
	e. Implementer associeringen giver, idet der kun er brug for at navigere fra gave til den der 
		giver. Bemærk, at retningspilen i forhold til dette ikke er tegnet på klassediagrammet, så 
		tegn først denne. 
	f. Tilføj nu en metode, der returnerer de personer, en person modtager gaver fra. Hvor skal 
	denne metode placeres? 
	g. Udvid afprøvningsklassen, så personer nu også giver de oprettede gaver. Afprøv dernæst 
	metoden fra opgave f).
	
	 */
	public static void main(String[] args) {

		// Test data
		Person p1 = new Person("Mads", 67);
		Person p2 = new Person("Søren", 16);
		Gift racerbil = new Gift("Rød racerbil", p1);
		racerbil.setPrice(150.0);
		Gift dukke = new Gift("Barbie", p1);
		dukke.setPrice(200);
		Gift vandflaske = new Gift("Vandflaske", p2);
		vandflaske.setPrice(15);

		// d)
		p1.giveGift(racerbil, p2);
		p1.giveGift(dukke, p2);
		p2.giveGift(vandflaske, p1);
		System.out.println(p1);
		System.out.println(p2);

		// f) 
		System.out.println(p1.getName() + " has recieved from : ");
		for (Person giver : p1.getGiftGivers()) {
			System.out.println("- " + giver.getName());
		}
		System.out.println(p2.getName() + " has recieved from : ");
		for (Person giver : p2.getGiftGivers()) {
			System.out.println("- " + giver.getName());
		}
	}

}
