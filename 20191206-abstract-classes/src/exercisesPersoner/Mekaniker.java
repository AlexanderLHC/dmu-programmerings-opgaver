package exercisesPersoner;

import java.time.LocalDate;
import java.util.Date;

public class Mekaniker extends Ansat {
	private LocalDate svendeprøveDate;
	private static Double timeløn = 172.0; // kr
	private static Double arbejdstimer = 37.0; 
	
	public Mekaniker(String navn, String adresse, LocalDate svendePrøveDate) {
		super(navn, adresse, timeløn, arbejdstimer);
		this.svendeprøveDate = svendePrøveDate;
	}

	public LocalDate getSvendeprøveDate() {
		return svendeprøveDate;
	}	
	
	@Override
	public double ugeLøn() {
		return timeløn*arbejdstimer;
	}
	
	@Override
	public String toString() {
		return super.toString() + String.format(" %-10s | %-5skr | %-10skr |", svendeprøveDate, timeløn, ugeLøn());
	}
}