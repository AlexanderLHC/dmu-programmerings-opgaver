package exercisesPersoner;

public abstract class Ansat extends Person{
	private double timeløn;
	private double arbejdsTimer;
	
	public Ansat(String navn, String adresse, double timeløn, double arbejdsTimer) {
		super(navn, adresse);
		this.timeløn = timeløn;
		this.arbejdsTimer = arbejdsTimer;
	}
	
	public double getTimeløn() {
		return timeløn;
	}
	
	public void setTimeløn(Double timeløn) {
		this.timeløn = timeløn;
	}
	
	public double getArbejdsTimer() {
		return arbejdsTimer;
	}
	
	public abstract double ugeLøn();
}
