package ex4figurer;

import java.util.ArrayList;

public class FigurTester {
	
	public static void main(String[] args) {
		ArrayList<Figur> figurer = new ArrayList<Figur>();
		figurer.add(new Cirkel(10, 10, 10));
		figurer.add(new Ellipse(20, 20, 20, 20));
		figurer.add(new Firkant(30, 30, 30));
		figurer.add(new Firkant(40, 40, 40, 40));

		for (Figur f : figurer) {
			System.out.println(f);
		}

	}
}
