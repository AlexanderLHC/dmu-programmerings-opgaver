package ex4;

public class Sudoku {
	private static boolean hasErrorInRow;
	private static boolean hasErrorInColumn;
	private static boolean hasErrorInQuadrant;

	private static int[] board = new int[] {
			5, 9, 1, 8, 4, 2, 7, 3, 9,
			3, 9, 8, 6, 1, 7, 4, 5, 2,
			2, 7, 4, 5, 3, 9, 1, 8, 6,
			4, 5, 6, 9, 7, 1, 3, 2, 8,
			9, 2, 7, 3, 8, 6, 5, 1, 4,
			1, 8, 3, 2, 5, 4, 6, 9, 7,
			6, 1, 5, 7, 9, 8, 2, 4, 3,
			8, 4, 2, 1, 6, 3, 9, 7, 5,
			7, 3, 9, 4, 2, 5, 8, 6, 1
	};
	// CORRECT
	//	private static int[] board = new int[] {
	//			5, 6, 1, 8, 4, 2, 7, 3, 9,
	//			3, 9, 8, 6, 1, 7, 4, 5, 2,
	//			2, 7, 4, 5, 3, 9, 1, 8, 6,
	//			4, 5, 6, 9, 7, 1, 3, 2, 8,
	//			9, 2, 7, 3, 8, 6, 5, 1, 4,
	//			1, 8, 3, 2, 5, 4, 6, 9, 7,
	//			6, 1, 5, 7, 9, 8, 2, 4, 3,
	//			8, 4, 2, 1, 6, 3, 9, 7, 5,
	//			7, 3, 9, 4, 2, 5, 8, 6, 1
	//	};

	public static void main(String[] args) {
		checkRows();
		checkColoumns();
		checkQuadrants();
	}

	/**
	 * 
	 * @param numberPile : an array of 9 numbers (either a row, column or quadrant)
	 * @param location   : checking row/column/quadrant (for printing error)
	 */
	public static boolean checkSudokoPart(int[] numberPile) {
		boolean partHasError = false;
		boolean isFound = false;
		int i = 1;
		while (!isFound && i <= 9) {
			for (int numberInCell : numberPile) {
				if (i == numberInCell) {
					isFound = true;
				}
			}
			if (!isFound) {
				partHasError = true;
			}
			i++;

		}
		return partHasError;
	}

	public static void checkValidInt() {
	}

	public static void checkRows() {
		System.out.printf("Checking rows: ");
		int[] part = new int[9];
		for (int row = 0; row < 9; row++) {
			for (int col = 0; col < 9; col++) {
				part[col] = board[(row * 9) + col];
			}
			if (checkSudokoPart(part)) {
				hasErrorInRow = true;
				System.out.printf("%n Error in row: " + (row + 1));
			}
		}
		if (!hasErrorInRow) {
			System.out.printf("GOOD!%n");
		}
	}

	/**
	 * 
	 * @param number quadrant number range from 0-8
	 * @return
	 */
	public static int[] getQuadrant(int number) {
		int[] quadrant = new int[9];
		int quadrantCurrentSize = 0;
		int rowsModifer = number / 3;
		int colsModifier = number * 3 % 9;
		for (int row = 0; row < 3; row++) {
			for (int col = 0; col < 3; col++) {
				quadrant[quadrantCurrentSize] = board[(row * 9) + col];
				quadrantCurrentSize++;
			}
		}
		return quadrant;
	}

	public static void checkQuadrants() {
		System.out.printf("Checking quadrants: ");
		for (int quadrant = 0; quadrant < 9; quadrant++) {
			if (checkSudokoPart(getQuadrant(quadrant))) {
				hasErrorInQuadrant = true;
				System.out.printf("%nError in quadrant: " + (quadrant + 1));
			}

		}
		if (!hasErrorInQuadrant) {
			System.out.printf("GOOD!%n");
		}
	}

	public static void checkColoumns() {
		System.out.printf("Checking columns: ");
		int[] numberPile = new int[9];
		for (int col = 0; col < 9; col++) {
			for (int row = 0; row < 9; row++) {
				numberPile[row] = board[(row * 9) + col];
			}
			if (checkSudokoPart(numberPile)) {
				hasErrorInColumn = true;
				System.out.printf("%nError in column: " + (col + 1));
			}
		}
		if (!hasErrorInColumn) {
			System.out.printf("GOOD!%n");
		}

	}
}
