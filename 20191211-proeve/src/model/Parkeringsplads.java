package model;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;

public class Parkeringsplads {
	private int nummer;
	private LocalTime ankomst;
	private Parkeringshus parkeringsHus;
	private Bil bil;
	private final double PRICE = 6; // per started 10min
	
	// TODO: tag parkeringshus i konstrukter
	public Parkeringsplads(int nummer) {
		ankomst = null;
		this.nummer = nummer;
	}
	
	public Parkeringsplads(int nummer, Bil bil) {
		this(nummer);
		this.bil = bil;
	}
	
	public int getNummer() {
		return nummer;
	}
	
	public void setNummer(int nummer) {
		this.nummer = nummer;
	}
	
	public LocalTime getAnkomst() {
		return ankomst;
	}
	
	public void setAnkomst(LocalTime ankomst) {
		this.ankomst = ankomst;
	}

	public Parkeringshus getParkeringsHus() {
		return parkeringsHus;
	}

	public void setParkeringsHus(Parkeringshus parkeringsHus) {
		if (this.parkeringsHus != parkeringsHus) {
			this.parkeringsHus = parkeringsHus;
		}
	}

	public Bil getBil() {
		return bil;
	}

	public void setBil(Bil bil) {
		if (this.bil != bil) {
			ankomst = LocalTime.now();
			this.bil = bil;	
		} 
	}
	
	// Nogle lagde den i Pakeringshus med Plads som param, 
	// "Bør" høre til herinde.
	// Mange har:
	// - fået timer
	// - fået min
	public double getPris(LocalTime finishedParking) {
		long minutesBetween = ChronoUnit.MINUTES.between(ankomst, finishedParking);
		return (Math.ceil(minutesBetween/10)+1)*PRICE;
	}
	
	public void hentBil(LocalTime afgangstidpunkt) {
		parkeringsHus.insertSaldo(getPris(afgangstidpunkt));
		setBil(null);
	}
}
