package controller;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;

import model.Bil;
import model.Bilmærke;
import model.Parkeringshus;
import model.Parkeringsplads;
import storage.Storage;

public class Controller {

	// ------------------- Parkeringshus -------------------
	public static Parkeringshus createParkeringshus(String adresse) {
		Parkeringshus parkeringshus = new Parkeringshus(adresse);
		Storage.addParkeringshus(parkeringshus);
		return parkeringshus;
	}
	public static ArrayList<Parkeringshus> getParkeringshuse(){
		return new ArrayList<Parkeringshus>(Storage.getParkeringshuse());
	}
	// ------------------- Parkeringsplads -------------------
	public static Parkeringsplads createParkeringsplads(Parkeringshus pHus, int nummer) {
		Parkeringsplads pPlads = pHus.createParkeringsplads(nummer);
		return pPlads;		
	}
	// Invalid uden areal
	public static Parkeringsplads createParkeringsplads(Parkeringshus pHus, int nummer, double areal) {
		Parkeringsplads pPlads = pHus.createParkeringsplads(nummer, areal);
		return pPlads;		
	}
	
	// ------------------- Bil -------------------
	public static Bil createBil(String regNr, Bilmærke mærke) {
		Bil bil = new Bil(regNr, mærke);
		Storage.addBil(bil);
		return bil;
	}
	// ------------------- Test -------------------
	public static void createSomeObjects() {
		Bil b1 = Storage.createBil("AB 11 222", Bilmærke.AUDI);
		Bil b2 = createBil("EF 33 444", Bilmærke.SKODA);
		Bil b3 = createBil("BD 55 666", Bilmærke.TESLA);

		Parkeringshus pHus1 = Controller.createParkeringshus("Havnegade 12, 8000 Aarhus");

		Parkeringshus pHus2= Controller.createParkeringshus("Månen 1, Universet");

		// 100 parkerings pladser
		for (int i = 1; i <= 100; i++) {
			pHus1.createParkeringsplads(i);
			pHus2.createParkeringsplads(i);
		}
		// 10 invalide parkerings pladser
		for (int i = 101; i <=110; i++) {
			pHus1.createParkeringsplads(i, 18.0);
			pHus2.createParkeringsplads(i, 18.0);
		}	
		
		pHus1.getParkeringspladser().get(1).setBil(b1);
		pHus1.getParkeringspladser().get(2).setBil(b2);		
		pHus1.getParkeringspladser().get(108).setBil(b3); // invalidepladser
	}
	
	// ------------------- Output -------------------
	public static void writeOptagnePladser(Parkeringshus hus, String filnavn) {
		try (PrintWriter writer = new PrintWriter(filnavn)) {
			for (String plads : hus.optagnePladser()) {
				writer.println(plads);
			}
		} catch (FileNotFoundException e) {
			System.out.println("Fil ikke fundet");
		} catch (Exception e) {
			System.out.println("Ukendt fejl!");
		}
	}
	
}
