package factory;

import java.util.Random;

public class Factory {
	
	public static int evenOrOdd(int chanceOdd) {
		Random random = new Random();
	    int n = random.nextInt(10);
	    int r = 2 * n ;
	    if (chanceOdd > random.nextInt(100)) {
	    	r+=1;
		}
	    return r;
	}
}
