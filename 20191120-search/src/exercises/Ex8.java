package exercises;

import java.util.ArrayList;
import java.util.Arrays;

public class Ex8 {

	/*
	 * Exercise 8 Write a linear search method that finds a number in an array. If
	 * the number does not exists the method returns -1. If the number is found, it
	 * should swap place with the number before, and return the new index of the
	 * number. In case the number is found on index 0, it should not swap place, but
	 * just return 0.
	 * 
	 * Example: Array before searching for 13: | 6 | 4 | 8 | 13 | 7 | Array after
	 * searching 13: | 6 | 4 | 13 | 8 | 7 | In this case the method returns 2.
	 * Repeat the exercise with an ArrayList instead of an array.
	 */

	public static void main(String[] args) {
		int[] l1 = { 6, 4, 8, 13, 7 };
		ArrayList<Integer> l2 = new ArrayList<>();
		for (int e : l1) {
			l2.add(e);
		}
		System.out.println(Arrays.toString(l1));

		int target = 13;
		System.out.printf("Lookup in array %s gives: %s%n", target, linMoveTargetOneLeftGetNewIndex(l1, target));
		target = 1;
		System.out.printf("Lookup in array %s gives: %s%n", target, linMoveTargetOneLeftGetNewIndex(l1, target));

		target = 13;
		System.out.printf("Lookup in arraylist %s gives: %s%n", target, linMoveTargetOneLeftGetNewIndex(l2, target));
		target = 1;
		System.out.printf("Lookup in arraylist %s gives: %s%n", target, linMoveTargetOneLeftGetNewIndex(l2, target));

	}

	public static int linMoveTargetOneLeftGetNewIndex(int[] list, int target) {
		int index = -1;

		int i = 0;
		while (i < list.length) {
			if (list[i] == target) {
				if (i != 0) {
					int temp = list[i];
					list[i] = list[i - 1];
					list[i - 1] = temp;
				}
				index = i;
			}
			i++;
		}
		return index;
	}

	public static int linMoveTargetOneLeftGetNewIndex(ArrayList<Integer> list, int target) {
		int index = -1;

		int i = 0;
		while (i < list.size()) {
			if (list.get(i) == target) {
				if (i != 0) {
					int temp = list.get(i);
					list.set(i, list.get(i - 1));
					list.set(i - 1, temp);
				}
				index = i;
			}
			i++;
		}
		return index;
	}
}
