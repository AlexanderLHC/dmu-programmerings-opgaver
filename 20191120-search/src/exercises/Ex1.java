package exercises;

import java.util.Arrays;

import factory.Factory;

public class Ex1 {

	public static void main(String[] args) {
		int[] list1 = getArray(10, 25);
		int[] list2 = getArray(5, 75);
		int[] list3 = {1, 3, 5, 9 }; 

		System.out.println("First attempt: " + Arrays.toString(list1)+ " = even found " + 
				linearSearchEvenessList(list1, true));

		System.out.println("Second attempt: " + Arrays.toString(list1)+ " = odd found " + 
				linearSearchEvenessList(list1, false));

		System.out.println("Third attempt: " + Arrays.toString(list2)+ " = even found " + 
				linearSearchEvenessList(list2, true));

		System.out.println("Third attempt: " + Arrays.toString(list2)+ " = odd found " + 
				linearSearchEvenessList(list2, false));

		System.out.println("Fourth attempt: " + Arrays.toString(list3)+ " = even found " + 
				linearSearchEvenessList(list3, true));

		System.out.println("Fourth attempt: " + Arrays.toString(list3)+ " = odd found " + 
				linearSearchEvenessList(list3, false));
	}
	
	private static int[] getArray(int length, int chanceOdd) {
		int[] list = new int[length];
		for (int i = 0; i < length; i++) {
			list[i] = Factory.evenOrOdd(chanceOdd);
		}
		return list;
	}
	
	private static boolean linearSearchEvenessList(int[] list, boolean isEven) {
		boolean found = false;
		int i = 0;
		while (i < list.length && !found) {
			if ((list[i] % 2 == 0) == isEven) {
				found = true;
			}
			i++;
		}
		return found;
	}

}