package exercises;

import com.github.javafaker.Faker;

public class Player implements Comparable<Player> {
	private String name;
	private int height;
	private int weight;
	private int scoredGoals;

	public Player() {
		Faker f = new Faker();
		this.name = f.name().firstName();
		this.height = f.random().nextInt(80, 200);
		this.weight = f.random().nextInt(50, 110);
		this.scoredGoals = f.random().nextInt(0, 100);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	public int getScoredGoals() {
		return scoredGoals;
	}

	public void setScoredGoals(int scoredGoals) {
		this.scoredGoals = scoredGoals;
	}

	@Override
	public String toString() {
		return String.format("%s (%scm/%skg) scored %s goals.", name, height, weight, scoredGoals);
	}

	@Override
	public int compareTo(Player p) {
		return p.scoredGoals - scoredGoals;
	}
}
