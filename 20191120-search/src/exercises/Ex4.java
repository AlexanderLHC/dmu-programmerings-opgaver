package exercises;

import java.util.ArrayList;
import java.util.Collections;

public class Ex4 {
	/*
	 * In this exercise you must use a Player class. Make a player class with the
	 * following fields: name, height, weight, scoredGoals. The class must have a
	 * constructor that initializes all the fields, and get methods for all four
	 * fields (and a toString() method). In TestSearching make an ArrayList of
	 * Player with about 6-8 Players.
	 * 
	 * a) Write a linear search method that returns a player with a given number of
	 * scored goals found in an ArrayList of players given as parameter. Return
	 * null, if no such player is found.
	 * 
	 * The header of the method: public Player findPlayerLinear(ArrayList<Player>
	 * players, int score)
	 * 
	 * b) Write a binary search method that returns a player with a given number of
	 * scored goals found in a sorted ArrayList of players (sorted in descending
	 * order according to scored goals) given as parameter. Return null, if no such
	 * player is found. The header of the method: public Player
	 * findPlayerBinary(ArrayList<Player> players, int score)
	 * 
	 * c) Write a linear search method that returns the name of a player with a
	 * height less than 170 cm and with more than 50 scored goals in a given
	 * ArrayList of players. Return the empty string, if no player is found. The
	 * header of the method: public String findPlayerName(ArrayList<Player> players)
	 * 
	 */

	public static void main(String[] args) {
		ArrayList<Player> players = new ArrayList<Player>();

		for (int i = 0; i < 8; i++) {
			players.add(new Player());
		}
		// a)
		int score = 8;
		players.get(2).setScoredGoals(score);
		System.out.printf("a) Linear find player with %s goals: %s.%n", score, findPlayerLinear(players, score));
		score = 99;
		System.out.printf("a) Linear find player with %s goals: %s.%n", score, findPlayerLinear(players, score));
		// b)
		Collections.sort(players);
		score = 8;
		System.out.printf("b) Binary find player with %s goals: %s.%n", score, findPlayerBinary(players, score));
		score = 99;
		System.out.printf("b) Binary find player with %s goals: %s.%n", score, findPlayerBinary(players, score));
		// c)
		System.out.printf("c) Linear search player for height < 170 and scored > 50 goals: %s.%n",
				findPlayerName(players));
		players.get(2).setHeight(160);
		players.get(2).setScoredGoals(55);
		System.out.printf("c) Linear search player for height < 170 and scored > 50 goals: %s.%n",
				findPlayerName(players));

	}

	public static Player findPlayerLinear(ArrayList<Player> players, int score) {
		Player pFound = null;

		int i = 0;
		while (i < players.size() && pFound == null) {
			if (players.get(i).getScoredGoals() == score) {
				pFound = players.get(i);
			}
			i++;
		}

		return pFound;
	}

	public static Player findPlayerBinary(ArrayList<Player> players, int score) {
		Player pFound = null;
		int left = 0;
		int right = players.size() - 1;

		while (left <= right && pFound == null) {
			int middle = (left + right) / 2;
			if (players.get(middle).getScoredGoals() == score) {
				pFound = players.get(middle);
			}
			if (players.get(middle).getScoredGoals() > players.get(left).getScoredGoals()) {
				left = middle + 1;
			} else {
				right = middle - 1;
			}
		}
		return pFound;
	}

	public static String findPlayerName(ArrayList<Player> players) {
		String name = "";
		int i = 0;
		while (i < players.size() && name == "") {
			if (players.get(i).getHeight() < 170 && players.get(i).getScoredGoals() > 50) {
				name = players.get(i).getName();
			}
			i++;
		}

		return name;
	}

}
