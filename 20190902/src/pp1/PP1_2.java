package pp1;

public class PP1_2 {

	/*
	 * # P1.2
	 * 
	 * You want to find out which fraction of your car’s use is for commuting to
	 * work, and which is for personal use. You know the one-way distance from your
	 * home to work. For a particular period, you recorded the beginning and ending
	 * mileage on the odometer and the number of work days. Write an algorithm to
	 * settle this question.
	 */

	public static void main(String[] args) {

		double distanceHomeToWork = 31; // miles
		double odometerMileageBeg = 1832; // miles measured beginning
		double odometerMileageEnd = 7692; // miles measured end
		double distanceDriven = odometerMileageEnd - odometerMileageBeg; // miles
		double workDays = 5 * 2 * 8; // 5 days over period of 8 weeks (x2 back and forth)

		double workRelatedCommute = workDays * distanceHomeToWork; // miles
		double personalRelatedCommute = distanceDriven - workRelatedCommute; // miles

		double workRatio = workRelatedCommute / distanceDriven * 100;
		double personalRatio = personalRelatedCommute / distanceDriven * 100;

		System.out.printf(
				"Total rejseafstand: %.1f.\nArbejds rejseafstand %.1f:%.0f%%\nPersonlig rejseafstand %.1f:%.0f%%\n",
				distanceDriven, workRelatedCommute, workRatio, personalRelatedCommute, personalRatio);

	}

}
