package pp1;

public class PP1_3 {

	/*
	 * # P1.3
	 * 
	 * The value of π can be computed according to the following formula:
	 * 
	 * π/4 = 1 - 1/3 + 1/5 - 1/7 + 1/9 - ...
	 * 
	 * Write an algorithm to compute π. Because the formula is an infinite series
	 * and an algorithm must stop after a finite number of steps, you should stop
	 * when you have the result determined to six significant digits.
	 * 
	 * π = 3.1415926536 (rounded)
	 */

	public static void main(String[] args) {
		boolean isPositive = true; // skifter efter hver løkke
		double numerator = 1;
		double denominator = 1;
		double sumRhs = 0; // højre side af lighedstegnet (right hand side)
		int attempts = 0; // forsøg brugt til beregning (fun factor = 9001)
		double pi = 3.141592;
		double piCalc = 0;

		do {
			if (isPositive) {
				sumRhs += numerator / denominator;
			} else {
				sumRhs -= numerator / denominator;
			}
			piCalc = 4 * sumRhs;
			attempts += 1;
			isPositive = !isPositive; // division/addition udskifter
			denominator += 2;

		} while (Math.abs(pi - piCalc) > 0.0000001);

		System.out.printf("Pi calculated as: %.6f. In %d attempts.", piCalc, attempts);
	}

}
