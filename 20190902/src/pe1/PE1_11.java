package pe1;

public class PE1_11 {

	public static void main(String[] args) {
		System.out.println("_________________________");
		System.out.println("< Hello Master Programmer >");
		System.out.println(" -------------------------");
		System.out.println("   \\");
		System.out.println("    \\");
		System.out.println("        .--.");
		System.out.println("       |o_o |");
		System.out.println("       |:_/ |");
		System.out.println("      //   \\ \\");
		System.out.println("     (|     | )");
		System.out.println("    /'\\_   _/`\\");
		System.out.println("   \\___)=(___/");
		System.out.println("");
	}

}
