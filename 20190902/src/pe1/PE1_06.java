package pe1;

public class PE1_06 {
	public static void main(String[] args) {

		System.out.println("___________");
		System.out.println("< ALEXANDER >");
		System.out.println(" -----------");
		System.out.println("      \\                    / \\  //\\");
		System.out.println("       \\    |\\___/|      /   \\//  \\\\");
		System.out.println("            /0  0  \\__  /    //  | \\ \\");
		System.out.println("           /     /  \\/_/    //   |  \\  \\");
		System.out.println("           @_^_@'/   \\/_   //    |   \\   \\");
		System.out.println("           //_^_/     \\/_ //     |    \\    \\");
		System.out.println("        ( //) |        \\///      |     \\     \\");
		System.out.println("      ( / /) _|_ /   )  //       |      \\     _\\");
		System.out.println("    ( // /) '/,_ _ _/  ( ; -.    |    _ _\\.-~        .-~~~^-.");
		System.out.println("  (( / / )) ,-{        _      `-.|.-~-.           .~         `.");
		System.out.println(" (( // / ))  '/\\      /                 ~-. _ .-~      .-~^-.  \\");
		System.out.println(" (( /// ))      `.   {            }                   /      \\  \\");
		System.out.println("  (( / ))     .----~-.\\        \\-'                 .~         \\  `. \\^-.");
		System.out.println("             ///.----..>        \\             _ -~             `.  ^-`  ^-_");
		System.out.println("               ///-._ _ _ _ _ _ _}^ - - - - ~                     ~-- ,.-~");
		System.out.println("                                                                  /.-~");

	}
}
