package re1;

/*
	- Find fejl:
		1. Klassen navngivet andet end filen
		2. println tager ikke parametre. Adskilles med + eller samles under en ".
public class >>Test<<[1]
{
   public static void main(String[] args)
   {
      System.out.println("Hello">>,<<[2] "World!");
   }
}
 */

public class R1_9 {

	public static void main(String[] args) {
		System.out.println("Hello World!");
	}

}
