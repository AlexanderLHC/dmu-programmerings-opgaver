/**
 * 
 */
package ex2ex3ex4student;

/**
 * @author alexander
 *
 */
public class PersonTester {

	/**
	 * @param args
	 */
	/*
	 #	Exercise 2:
	  Add to the Person1 class a method calcAge(). The method must take three int parameters 
		representing year, month and day (as in 2010, 9, 13). The method must return the persons age 
		on the specified day in years.
		Test your method with several persons with different birthdays. 
	 #	Exercise 3:
	  Add to the Person class a method that returns whether a given year (like 2010) is a leap year. 
		The rules for leap years are:  
		A year divisible by 4 is a leap year, 
		but years divisible by 100 are not, 
		unless the year is divisible by 400. 
	 #	Exercise 4:
	  Add to the Person class a method that returns the weekday of the person's birthday in a given year.
	The method, called calcWeekday(), must 
		* As parameter have the weekday of the day the person is born (with type String). 
		* Also as parameter have a year (later than the year the person is born). 
		* Return the weekday (as a String) of the birthday in the year given as parameter. 
	 */
	public static void main(String[] args) {
		Person p1 = new Person("Curie", 2000, 01, 20);
		p1.calcAge();
		p1.calcAgeWithLib();
//		Person p2 = new Person("Leeter", 1337, 01, 12);
//		p2.calcAge();
//		p2.leapYeahParty(2000);
//		p2.leapYeahParty(1924);
//		p2.leapYeahParty(2020);
//		p2.leapYeahParty(1321);

		// EX 4:
		// 20 Jan 2000 = Torsdag
		// Expected = Mandag
		// System.out.println(p1.calcWeekday("torsdag", 2020));

	}

}
