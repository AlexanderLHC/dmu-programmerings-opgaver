/**
 * 
 */
package ex;

import java.util.Collections;

/**
 * @author alexander
 *
 */
public class P5_10_RomanNumbers {

	/**
	 * @param args
	 */
	/*
	 * Roman numbers. Write a program that converts a positive integer into the Roman number system.
	 * The Roman number system has digits
	 	I | 1
	 	V | 5
	 	X | 10
	 	L | 50
	 	C | 100
	 	D | 500
	 	M | 1.000
	 	
	 	1|9|9|8
	 	
	 	
	 	Numbers are formed according to the following rules:
	 	a) Only numbers up to 3.999 are represented.
	 	b) As in the decimal system, the thousands, hundreds, tens,
	 	and ones are expressed separately.
	 	c) The numbers 1 to 9 are expressed as
	 	
	 	I    | 1
	 	II   | 2
	 	III  | 3
	 	IV   | 4
	 	V    | 5
	 	VI   | 6
	 	VII  | 7
	 	VIII | 8
	 	IX   | 9
	
	 	As you can see, an I preceding a V or X is subtracted from the value,
	 	and now you can never have more than three I's in a row.
	 	d) Tens and hundreds are done the same way, except that the letters
	 	X, L, C and C, D, M are used instead of I, V, X respectively.
	 	
	 	Your program should take an input, such as 1978, and convert it to Roman numerals, MCMLXXVIII.
	 */
	public static void main(String[] args) {

		int number = 1978;
		System.out.print("År: " + number + " = ");
		intToRomanNum(number);

		number = 2018;
		System.out.print("\nÅr: " + number + " = ");
		intToRomanNum(number);

		System.out.print("\nJulius Cæsar blev: ");
		intToRomanNum(31);
		System.out.print(" år og døde: ");
		intToRomanNum(44);
		System.out.print(" f.Kr.");

	}

	public static void intToRomanNum(int number) {
		String romanNum = "";
		for (int i = 3; i >= 0; --i) {
			int digit = number / (int) (Math.pow(10, i)) % 10;
			if (i == 0) {
				digitToRomanNum(digit, "I", "V", "X");
			} else if (i == 1) {
				digitToRomanNum(digit, "X", "L", "C");
			} else if (i == 2) {
				digitToRomanNum(digit, "C", "D", "M");
			} else if (i == 3) {
				digitToRomanNum(digit, "M", "M", "M");
			}
		}

	}

	public static void digitToRomanNum(int digit, String modSmall, String modMedium, String modLarge) {
		String romanNum = "";
		if (digit <= 3) {
			romanNum = String.join("", Collections.nCopies(digit, modSmall));
		} else if (digit == 4) {
			romanNum = modSmall + modMedium;
		} else if (digit >= 5 && digit < 9) {
			romanNum = modMedium + String.join("", Collections.nCopies(digit - 5, modSmall));
		} else {
			romanNum = modSmall + modLarge;
		}
		System.out.print(romanNum);
	}
}
