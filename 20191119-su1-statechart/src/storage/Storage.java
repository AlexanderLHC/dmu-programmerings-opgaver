package storage;

import java.util.ArrayList;

import model.KontoType;

public class Storage {

	private static ArrayList<KontoType> kontoTyper = new ArrayList<KontoType>();

	public static void addKontoType(KontoType kontoType) {
		if (!kontoTyper.contains(kontoType)) {
			kontoTyper.add(kontoType);
		}
	}

}
