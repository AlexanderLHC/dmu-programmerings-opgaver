/**
 * 
 */
package ex;

/**
 * @author alexander
 *
 */
public class PE5_7 {

	/**
	 * @param args
	 */
	/*
	 * E5.7 Write a program that reads three numbers and prints “increasing” if they
	 * are in increasing order, “decreasing” if they are in decreasing order, and
	 * “neither” otherwise. Here, “increasing” means “strictly increasing”, with
	 * each value larger than its predecessor. The sequence 3 4 4 would not be
	 * considered increasing.
	 */
	public static void main(String[] args) {
		int i11 = 10, i12 = 100, i13 = 1000000000;
		System.out.println("------\n1: " + 1);
		mathChecker(i11, i12, i13);
		int i21 = 99, i22 = 32, i23 = 1;
		System.out.println("------\n1: " + 2);
		mathChecker(i21, i22, i23);
		int i31 = 31, i32 = 31, i33 = 31;
		System.out.println("------\n1: " + 3);
		mathChecker(i31, i32, i33);
	}

	public static void mathChecker(int i1, int i2, int i3) {
		if (i1 < i2 && i2 < i3) {
			System.out.println("Increasing");
		} else if (i1 > i2 && i2 > i3) {
			System.out.println("Decreasing");
		} else {
			System.out.println("Neither");
		}
	}

}
