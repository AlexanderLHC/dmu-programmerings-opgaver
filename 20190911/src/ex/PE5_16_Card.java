/**
 * 
 */
package ex;

/**
 * @author alexander
 *
 */
public class PE5_16_Card {
	private String cardNotation;

	/**
	 * @param args
	 */
	/*
	 * E5.16Write a program that takes user input describing a playing card in the
	 * following shorthand notation:
	 * 
	 * A Ace 2 ... 10 Card values J Jack Q Queen K King D Diamonds H Hearts S Spades
	 * C Clubs Your program should print the full description of the card. For
	 * example,
	 * 
	 * Enter the card notation: QS Queen of Spades Implement a class Card whose
	 * constructor takes the card notation string and whose getDescription method
	 * returns a description of the card. If the notation string is not in the
	 * correct format, the getDescription method should return the string "Unknown".
	 * 
	 * 
	 */
	public PE5_16_Card(String cardNotation) {
		this.cardNotation = cardNotation;
	}

	public void setCardNotation(String cardNotation) {
		this.cardNotation = cardNotation;
	}

	public String getDescription() {
//		d, h, s, c
		char suitNotation = cardNotation.charAt(cardNotation.length() - 1);
		String valueNotation = cardNotation.substring(0, cardNotation.length() - 1);
		String suit = "";
		String value = "";
		if (suitNotation == 'D') {
			suit = "Diamond";
		} else if (suitNotation == 'H') {
			suit = "Heart";
		} else if (suitNotation == 'S') {
			suit = "Spade";
		} else if (suitNotation == 'C') {
			suit = "Club";
		} else {
			suit = "Unknown";
		}

		if (valueNotation.equals("J")) {
			value = "Jack";
		} else if (valueNotation.equals("Q")) {
			value = "Queen";
		} else if (valueNotation.equals("K")) {
			value = "King";
		} else if (valueNotation.equals("A")) {
			value = "Ace";
		} else if (Integer.parseInt(valueNotation) >= 2 && Integer.parseInt(valueNotation) <= 10) {
			value = valueNotation;
		} else {
			value = "Unknown";
		}
		return value + " of " + suit;
	}
}
