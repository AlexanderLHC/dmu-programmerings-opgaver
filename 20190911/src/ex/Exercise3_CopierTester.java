/**
 * 
 */
package ex;

/**
 * @author alexander
 *
 */
public class Exercise3_CopierTester {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Exercise3_Copier copier1 = new Exercise3_Copier(400);
		copier1.makeCopy(11);
		copier1.insertPaper(200); // For mange sider indsat
		copier1.makeCopy(450); // For få sider i printeren
		copier1.insertPaper(100);
		copier1.makeCopy(450); // Print 450
		copier1.makePaperJam(); // Typisk jam
		copier1.makeCopy(2); // Print med papir sat fast
		copier1.fixJam(); // Ordn jam
		copier1.makeCopy(2); // Print med papir sat fast
	}

}
