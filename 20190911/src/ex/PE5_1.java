/**
 * 
 */
package ex;

import java.util.Scanner;

/**
 * @author alexander
 *
 */
public class PE5_1 {

	/*
	 * E5.1 Write a program that reads an integer and prints whether it is negative,
	 * zero, or positive.
	 */
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("Indtast et tal: ");
		int i1 = scan.nextInt();

		if (i1 > 0) { // is positive
			System.out.println("Have a good day");
		} else if (i1 < 0) { // is negative
			System.out.println("Negativity spreads like virus");
		} else { // is 0
			System.out.println("I have no legs");
		}
	}

}
